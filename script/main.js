$( document ).ready(function() {

	$('#navigation a').click(function(){
		$('#navigation a').removeClass('active');
		if($(this).hasClass('active')){
			$(this).removeClass('active');
		}else{
			$(this).addClass('active');
		}
	});

	$('#scroll-type').on('click',function(){
    	$('html, body').animate({scrollTop: $("#types-jobs").offset().top-60}, 700);
    });

    $('.request').on('click',function(){
    	$('html, body').animate({scrollTop: $("#types-jobs").offset().top-60}, 700);
    });

	$('#labor a').click(function(){
		$('#labor a').removeClass('act');
		if($(this).hasClass('act')){
			$(this).removeClass('act');
		}else{
			$(this).addClass('act');
		}
	});


    $('#toggle').click(function(){
        $('#middle-nav').slideToggle('.active');
    });


    /* Отправка формы */

    $('.event-sendform-questions').click(function(e){
        
        e.preventDefault;

        $(this).parent().find('.required').removeClass('error');
        var error = false;

        $(this).parent().find('.required').each(function(i){
        if($(this).val()=="") {
            error=true;
            $(this).addClass('error').parent().parent().parent().find('.warning-window').fadeIn(function(){
                $('.warning').addClass('act');
            });
            } 
        });

        $(this).parent().find('.form-control').removeClass('error');

        $(this).parent().find('.form-control').each(function(){
        if ($(this).val()=='0') {
            error=true;
            $(this).addClass('error');
        }
        });
        if(!error){
            var name=$('#name').val();
            var mail=$('#yourMail').val();
            var message=$('#sendMsg').val();
            $.get('/sendquestion.php', { name: name, email: mail, question: message},

                function(data) {
                    if(data=='1'){
                        $('#name, #yourMail, #sendMsg').val('');
                        alert('Ваш вопрос успешно отправлен');
                    } else {
                        alert('Произошла ошибка при отправке сообщения');
                }
            });
    }
            return false;
    });


    /* Проверка формы заявки */

    $('.event-sendform-cost').click(function(e){
        
        e.preventDefault;

        $(this).parent().parent().find('.required').removeClass('error');
        var error = false;

        $(this).parent().parent().find('.required').each(function(i){
        if($(this).val()=="") {
            error=true;
            $(this).addClass('error').parent().parent().parent().find('.warning-window').fadeIn(function(){
                $('.warning').addClass('act');
            });
            } 
        });

        $(this).parent().parent().find('.form-control').removeClass('error');

        $(this).parent().parent().find('.form-control').each(function(){
        if ($(this).val()=='0') {
            error=true;
            $(this).addClass('error');
        }
        });
        if(!error){
            $(this).parent().parent().submit();
    }
            return false;
    });




    $('.warning-window, .exit-warning').click(function(){
        $('.warning-window').fadeOut().end().find('.warning').removeClass('act');
    });

    /*конец проверки формы*/

    $('.form-control, .required').change(function(){

        if ($(this).val() !='0') {

            error=true;
            $(this).addClass('success');
        }
        });

    $('.question').click(function(){

        $(this).parent().find('.answer').slideToggle('.active');
    });
});


$(document).on('change','select#exampleInputType',function(){
    var sel = $("select#exampleInputType").val();

var pay='';
var unic='';

    $('.advantages .advantages-center').animate({left:"-=20"},100).animate({left:"+=30"},100).animate({left:"-=10"},100,function(){
        $.get('/ajx/get_dataTypeWork', { id: sel},
        function(data) {
            
            var obj = jQuery.parseJSON(data);
            console.log(obj);
            pay=obj.cena;
            unic=obj.origin;

            $('.advantages p').fadeIn();
            $('div.advantages .advantages-1 .advantages-center p').text(pay);
            $('div.advantages .advantages-3 .advantages-center p').text(unic);
        });
    });
});


/* Анимация для всплывающего окна */


$(document).on('change','select#mainForm',function(){
    var sel = $("select#mainForm").val();

var pay='';
var unic='';

    $('.advantages .advantages-center').animate({left:"-=20"},100).animate({left:"+=30"},100).animate({left:"-=10"},100,function(){
        $.get('/ajx/get_dataTypeWork', { id: sel},
        function(data) {
            
            var obj = jQuery.parseJSON(data);
            pay=obj.cena;
            unic=obj.origin;

            $('.advantages p').fadeIn();
            $('div.advantages .advantages-1 .advantages-center p').text(pay);
            $('div.advantages .advantages-3 .advantages-center p').text(unic);
        });
    });
});