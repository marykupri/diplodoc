<section id="textContainer">
	<div id="pageText">
		<div class="container">
		<h1 class="handyWorks-pagetitle"><?php echo $work_name; ?></h1><div class="handyWorks-breadcrumbs">
			<ul class="handyWorks-breadcrumbs-list">
				<li class="handyWorks-breadcrumbs-listitem"><a href="/">Главная</a> /</li>
				<li class="handyWorks-breadcrumbs-listitem"><a href="/workfine">Готовые работы</a> /</li>
				<li class="handyWorks-breadcrumbs-listitem"><?php echo $work_name; ?></li>
			</ul>
		</div>
		<div class="handyWorks-description">
			<b class="handyWorks-label">Оглавление работы:</b>
			<pre><?php echo $work_description; ?>
			</pre>
		</div>
		<div class="handyWorks-price">
			<b class="handyWorks-label">Стоимость:</b>
			<?php echo $work_price; ?>
		</div>
		<div class="handyWorks-form-wrapper">
			<b class="handyWorks-label">Заказать работу:</b>
			<form class="handyWorks-form">
				<input type="hidden" name="b-id" class="required" value="<?php echo $work_id; ?>"/>
				<input type="text" name="b-name" class="required" placeholder="Имя"/>
				<input type="text" name="b-mail" class="required" placeholder="E-mail"/>
				<input type="text" name="b-phone" class="phone-mask required" placeholder="Телефон"/>
				<textarea name="b-comment" class="like-input"></textarea>
				<div class="handyWorks-submit js-handyWorks-submit">Купить</div>
			</form>

			<script>
				$(document).ready(function(){
					// Отправка заявки на покупку готовой работы
					$('.js-handyWorks-submit').click(function(){
								
						        $('.handyWorks-form .required').removeClass('error');
						        var error = false;

						        $('.handyWorks-form .required').each(function(i){
							        if($(this).val()=="") {
							            	error=true;
							            	$(this).addClass('error');
							            } 
						        });

						   
						        if(!error){
						        	var bid=$('.handyWorks-form input[name="b-id"]').val();
						            var bname=$('.handyWorks-form input[name="b-name"]').val();
						            var bmail=$('.handyWorks-form input[name="b-mail"]').val();
						            var bphone=$('.handyWorks-form input[name="b-phone"]').val();
						            var bmessage=$('.handyWorks-form textarea[name="b-comment"]').val();
						            var btitle=$('.handyWorks-pagetitle').html();

						            $.get('/send-buy-handywork.php', { 
						            	bid: bid,
						            	title:btitle,
						            	name: bname, 
						            	email: bmail, 
						            	phone: bphone,
						            	comment: bmessage
						            },

						            function(data) {
						                if(data=='1'){
						                	console.log(data);
						                    $('.handyWorks-form input[type="text"], .handyWorks-form textarea').val('');
						                    alert('Ваш запрос отправлен, менеджер свяжется с Вами в ближайшее время');
						                } else {
						                    alert('Запрос не был отправлен. Повторите попытку еще раз.');
						            }
						        });
						    }
						            return false;
											});
				});
			</script>
		</div>
 	</div>
</section>