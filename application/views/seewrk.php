<?
  $tdListWorks = '';
  $optnListSctn = '';
  $optnListType='';
  $optnListAuthor='';
  $namework='';
  $descrwork='';
  $razdelwork='';
  $type='';
  $author='';
  $typework='';
  $authorwork='';
  $tagswork='';
  $idwork='';

  if(!empty($listWork))
    foreach ($listWork as $key => $value) {
      $namework=$value->name;
      $descrwork=$value->descr;
      $razdelwork=$value->razdel;
      $typework=$value->typew;
      $authorwork=$value->authorw;
      $type=$value->type;
      $author=$value->author;
      $tagswork=$value->tags;
      $idwork=$value->id;
    }

  if(!empty($listSections))
    foreach ($listSections as $key => $value) {
      $select = '';
      if($value->id == $razdelwork)
      $optnListSctn= $value->nazv;
    }

    if(!empty($listType))
    foreach ($listType as $key => $value) {
      $select = '';
      if($value->id == $type)
      $optnListType= $value->nazv;
    }

    if(!empty($listAuthor))
    foreach ($listAuthor as $key => $value) {
      $select = '';
      if($value->id == $author)
      $optnListAuthor= $value->name;
    }
?>

<div id="editForm">
  <form class="form-horizontal" method="post" action="/admin/editWork/<?php print $idwork;?>">
    <div class="form-group">
      <label for="inputEmail3" class="col-sm-2 control-label">Название работы</label>
      <div class="col-sm-10">
        <?php print $namework;?>
      </div>
    </div>
    <div class="form-group">
      <label for="inputPassword3" class="col-sm-2 control-label">Описание работы</label>
      <div class="col-sm-10">
        <textarea class="form-control" id="inputPassword3" rows="15" name="inputDescr"><?php print $descrwork;?></textarea>
      </div>
    </div>

    <div class="form-group">
      <label for="exampleInputType" class="col-sm-2 control-label">Тип работы</label>
      <div class="col-sm-10">
          <?php print $optnListType;?>
      </div>
    </div>    

    <div class="form-group">
      <label for="exampleInputRazdel" class="col-sm-2 control-label">Раздел</label>
      <div class="col-sm-10">
          <?php print $optnListSctn;?>
      </div>
    </div>
    <div class="form-group">
      <label for="exampleInputAuthor" class="col-sm-2 control-label">Автор</label>
      <div class="col-sm-10">
          <?php print $optnListAuthor;?>
      </div>
    </div>
    <div class="form-group">
      <label for="inputTags" class="col-sm-2 control-label">Теги</label>
      <div class="col-sm-10">
        <?php print $tagswork;?>
      </div>
    </div>

  </form>
</div>