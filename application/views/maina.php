<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="/css/stylea.css" type="text/css" >
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <!-- Optional theme -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
  <!-- Latest compiled and minified JavaScript -->
  <script src="http://yastatic.net/jquery/2.1.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

  <script type="text/javascript" src="/js/ckeditor/ckeditor.js"></script>
  <script type="text/javascript" src="/js/ckeditor/samples/js/sample.js"></script>
  <script type="text/javascript" src="/script/admin.js"></script>

  <title>Система управления сайтом</title>
</head>
<body>
	<div id="menu">
		<div id="logout"><a href="/admin/logOut">Выйти</a></div>
		<ul class="nav nav-pills">
		  <li><a href="/admin">База работ</a></li>
		  <li><a href="/admin/applicat">Заявки на работу</a></li>
		  <li><a href="/admin/textpage">Текстовые страницы</a></li>
		  <li><a href="/admin/typework">Типы работ</a></li>
		  <!-- <li><a href="/admin/subject">Предметы</a></li> -->
		  <li><a href="/admin/author">Авторы</a></li>
		  <li><a href="/admin/manager">Менеджеры</a></li>
		</ul>
	</div>
	<div id="content">	
		<?php print $content;?>
	</div>
</body>
</html>