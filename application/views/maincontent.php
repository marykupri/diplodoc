<section id="start-page">
	<div class="inside">

<div class="fotorama"  
     data-width="100%"
     data-autoplay="3000"
data-allowfullscreen="false"
     >

  <img src="image/girl.jpg" data-allowfullscreen="true" width="100%!important" ></img>
  <img src="image/man.jpg"></img>
  <img src="image/pay.jpg"></img>

</div>
					
</div>
</section>
<section id="types-jobs">
	<div class="about">
		<ul class="garantii">
		<div>
			<li class="block-question">
			<img  src="image/Hourglass.png" class="animated zoomIn "></img>
			<p class="bol">Выполнение заказа точно в срок</p> 
			<p class="warranty">Свой заказ вы получаете строго к сроку, который указываете при
			оформлении заявки;</p>
			</li>
			<li class="block-question">
			<img src="image/Theatre.png" class="animated zoomIn "></img>
			<p class="bol">Все доработки быстро и бесплатно</p> 
			<p class="warranty"> Сопровождение заказа до самой
			защиты. Все необходимые исправления по рекомендациям вашего
			преподавателя, не противоречащие первоначальным требованиям,
			бесплатны;</p>
			</li>
		</div>
		<div>
			<li class="block-question">
			<img src="image/Navigation.png" class="animated zoomIn "></img>
			<p class="bol">Оригинальность работы не менее 80%</p> 
			<p class="warranty">Каждая работа проходит проверку по уровню оригинальности текста.
			При оформлении заказа, вы можете самостоятельно указать требуемый
			процент оригинальности;</p>
			</li>
			<li class="block-question">
			<img src="image/Mouse.png" class="animated zoomIn "></img>
			<p class="bol">Отдел контроля и качества</p> 
			<p class="warranty">Все заказы проверяются нашими специалистами на соответствие всех
			требований по содержанию, оформлению и проценту оригинальности;</p>
			</li>
		</div>
		<div>
			<li class="block-question">
			<img src="image/Speedometer.png" class="animated zoomIn "></img>
			<p class="bol">Конфиденциальность</p> 
			<p class="warranty">Информация о наших клиентах и заказах остается только в нашей компании.</p>
			</li>
			<li class="block-question">
			<img src="image/Book.png" class="animated zoomIn "></img>
			<p class="bol">Заключение договора</p> 
			<p class="warranty">Надежные юридические гарантии вашего заказа.</p>
			</li>
		</div>
		</ul>
		<div class="content">
			<script>
				$(document).ready(function() {						
					$('#big-close, .overlay').click(function(){
						$('#myModalInfo').fadeOut();
						$('.overlay').fadeOut();
					});
				});
			</script>
			<?php
				if($addSuccess)
					print '
						<div class="overlay"></div>
						<div id="myModalInfo">
						<div id="big-close">x</div>
							<h2>Заявка принята.</h2>
        					<p>Ваша заявка принята. В ближайшее время мы вам позвоним для уточнения информации. Будьте на связи )</p>
 						</div>';
			?>
<div class="clearfix"></div>
<div class="application">
<h2>Информация о работе</h2>
<div class="advantages">
	<div class="advantages-1"><div class="advantages-center animated"><img src="image/pay.png"><p>От 500₽ </p></div></div>
	<div class="advantages-2"><div class="advantages-center animated"><img src="image/like.png"><p>Гарантия 1 год</p></div></div>
	<div class="advantages-3"><div class="advantages-center animated"><img src="image/doc1.png"><p>Оригинальность не менее 70%</p></div></div>
</div>

<form method="post" action="/sendorder" id="form1" name="form1">
	
<div class="null-line">
<span>Тип работы</span>
<select class="form-control" id="exampleInputType" name="BusinessDistrictId">
	<?print $listType;?>
</select>
</div>
<div class="null-line">
	<span>Специальность</span> 
	<input type="text" class="required" size="25" value="" name="nameSpecialty" id="nameSpecialty" placeholder="Прикладная информатика"/><br/>
</div>

<div class="null-line">
<span>Тема работы</span> 
<input type="text" class="required" size="25" value="" name="name-theme" id="name-theme" placeholder="Физика"/><br/>

</div>
<!-- <h3>Введите контактные данные. Мы гарантируем <span>конфиденциальность</span>.</h3> -->
    <div class="null-line">
    <input type="text" class="required" minlength="1" size="25" value="" name="number-pages" id="number-pages" placeholder="Кол-во страниц"/><br/>
	<input type="text" class="term-2 e-select-date-main" minlength="1" size="25" value="" name="yourname" id="yourname" placeholder="Срок сдачи"/><br/>
	</div>
	<div class="null-line">
	<span>Ваше имя</span><input type="text" class="required" minlength="1" size="25" value="" name="yourName" id="yourName" placeholder="Иван"/><br/>
	</div>
	<div class="null-line">
	<span>Электронная почта</span> <input type="text" class="required" minlength="1" size="25" value="" name="yourmail" id="yourmail" placeholder="ivanov@yahoo.com"/><br/>
	</div>
	<div class="null-line">
	<span>Телефон</span> <input type="tel" id="phone" class="required e-mask-phone" size="25" value="" name="phone" placeholder="+7(___) ___-____"/><br/>
			<script type="text/javascript">
   jQuery(function($){
   $(".e-mask-phone").mask("+7(999) 999-9999");
   });
</script>
	</div>
	
	<div class="null-line">
	<span>Комментарий</span>
	<textarea rows="10" cols="50" name="msg" id="inputMsg"></textarea>
	</div>
	<div class="clearfix"></div>
		<div class="file_upload">
		<!-- <button type="button" class="add-file">Прикрепить файл</button> -->
		<label for="inputFile" class="uploadButton"><p>Прикрепить материал</p><img src="image/clip.png" width="20px;"></img></label>
		<input style="opacity: 0; cursor:pointer;" type="file" name="inputFile">
	<div class="successUpld"></div>
	<input type="hidden" id="typeForm" value="order" name="typeForm" />
	<button type="submit" name="insFeed" id="insFeed" value="insFeed" class="cost event-sendform-cost">Узнать стоимость</button>
	<!--<button type="file"class="add-file">Прикрепить файл</button>-->
</div>
</form>
</div>
					</div>
				</div>
			</section>


			<section id="pay">
				<h2>Способы оплаты</h2>
<ul class="payment">
	<div>
	<li class="block-question">
		<img src="image/rub.svg"></img>
		<p class="bol">Наличными в офисе</p> 
		<p class="warranty1">Внести денежные средства можно в Центральном офисе в г. Москве и в г.Волгограде, в офисах других городов по предварительному согласованию с вашим менеджером.</p>
	</li>
	<li class="block-question">
		<img src="image/alpha.svg"></img>
		<p class="bol">Расчетный счет, Альфа-Клик</p> 
		<p class="warranty1">Самая универсальная система оплаты - перевод на расчетный счет компании. Вы можете оплатить как в отделении любого банка, на почте, а также через Альфа-Клик.</p>
	</li>
	</div>
	<div>
	<li class="block-question">
		<img src="image/ya.svg"></img>
		<p class="bol">Система Яндекс.Деньги</p> 
		<p class="warranty1">Если у вас есть электронный кошелек <a href="https://money.yandex.ru/new" target="_blank">Яндекс.Деньги</a>, то Вы можете легко и быстро перевести деньги внутри системы на наш счет.</p>
	</li>
	<li class="block-question">
		<img src="image/card.svg"></img>
		<p class="bol">Банковские карты</p> 
		<p class="warranty1">Вы можете оплатить наши услуги со своей банковской карты через online банк. Оплата услуг производится через банки, находящиеся на территории России.</p>
	</li>
	</div>
	<div>
	<li class="block-question">
		<img src="image/sber.svg"></img>
		<p class="bol">Расчетный счет, Сбербанк Онлайн</p> 
		<p class="warranty1">Для вашего удобства у нас также есть расчетный счет в банке Сбербанк России. Вы также можете воспользоваться системой Сбербанк Онлайн.</p>
	</li>
	<li class="block-question">
		<img src="image/web.svg"></img>
		<p class="bol">Система WebMoney</p> 
		<p class="warranty1">Если же у вас нет кошелька в системе WebMoney, 
		то завести его можно очень просто за 5 минут, инструкции вы можете найти на <a href="http://www.webmoney.ru/" target="_blank">официальном сайте</a>.</p>
	</li>
	</div>
</ul>
		    </section>
			
			</section>