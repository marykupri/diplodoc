<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=no,maximum-scale=1.0">

	<title>Hellodiplom. Дипломы, курсовые, рефераты и другие типы работ.</title>

	<link href='//fonts.googleapis.com/css?family=Roboto:400,700,300,500,400italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
	<link href="css/reset.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link href="css/animate.css" rel="stylesheet"/>
	<link href="//cdnjs.cloudflare.com/ajax/libs/fotorama/4.5.1/fotorama.css" rel="stylesheet">
    <link href="./css/jquery-ui.structure.min.css" rel="stylesheet">
    <link href="./css/jquery-ui.theme.min.css" rel="stylesheet">
	<link href="./css/jquery-ui.min.css" rel="stylesheet">

	<link rel="shortcut icon" href="/image/favicon.png" type="image/png">

	<script src='//code.jquery.com/jquery-2.1.1.min.js'></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/fotorama/4.5.1/fotorama.js"></script>
	<script src="js/maskedinput.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<script src="script/main.js"></script>
	<script src="js/ru.js"></script>

</head>
<body>
	<div class="wrapper">
	<aside>
		<a href="/"><img src="image/logo.svg" alt="Hellodiplom - дипломы и курсовые работы на заказ" class="animated zoomIn "></a>
		<div id="toggle">
			<div class="sandwich-toggle">
				
			</div>
		</div>
		<ul id="middle-nav">
			<li><a href="/about" data-id="1">О компании</a></li>
			<li><a href="/services" data-id="3">Услуги</a></li>
			<li><a href="/guarantee" data-id="7">Оплата</a></li>
			<li><a href="/workcond" data-id="4">Условия работы</a></li>
			<li><a href="/quesansw" data-id="8">Вопросы и ответы</a></li>
			<li><a href="/workfine" data-id="6">Готовые работы</a></li>
			<li><a href="/contacts" data-id="5">Контакты</a></li>
			<li><a href="/cooperation" data-id="2">Сотрудничество</a></li>
		</ul>
		<ul id="navigation">
			<li><a href="/about" data-id="1">О компании</a></li>
			<li><a href="/services" data-id="3">Услуги</a></li>
			<li><a href="/guarantee" data-id="7">Оплата</a></li>
			<li><a href="/workcond" data-id="4">Условия работы</a></li>
			<li><a href="/quesansw" data-id="8">Вопросы и ответы</a></li>
			<li><a href="/workfine" data-id="6">Готовые работы</a></li>
			<li><a href="/contacts" data-id="5">Контакты</a></li>
			<li><a href="/cooperation" data-id="2">Сотрудничество</a></li>
		</ul>
		<div class="social-block">
			<p>Мы в социальных сетях:</p>
			<nav class="social">
				<a href="#" class="vkontakte"></a>
				<a href="#" class="facebook"></a>
				<!--<a href="#" class="twitter"></a>
				<a href="#" class="inst"></a>-->
				<a href="#" class="ok"></a>
			</nav>	
		</div>
	</aside>
		<div class="main">
			<header>
				<ul class="call">
					<li id="callback"><p>Заказать</p></li>
					<li><span class="lptracker_phone">79199854097</span></li>
				</ul>
			</header>

<?print $content;?>

		</div>

		<div id="modal-window" class="modal-window">

		<div class="window-container animation">
			
		<div class="application wind">
											

<h2>Информация о работе</h2>
<div class="close"></div>
<div class="advantages advantagesMain">

	<div class="advantages-1"><div class="advantages-center"><img src="image/pay.png"><p>От 500₽</p></div></div>
	<div class="advantages-2"><div class="advantages-center"><img src="image/like.png"><p>Гарантия 1 год</p></div></div>
	<div class="advantages-3"><div class="advantages-center"><img src="image/doc1.png"><p>Оригинальность не менее 70%</p></div></div>
</div>

<form method="post" action="/sendorder" id="form2" name="form2">
	
<div class="null-line">
<span>Тип работы</span>
	<select class="form-control" id="mainForm" name="BusinessDistrictId">
		<?print $listType;?>
	</select>
</div>
<div class="null-line">
	<span>Специальность</span> <input type="text" class="required" size="25" value="" name="nameSpecialty" id="nameSpecialty" placeholder="Прикладная информатика"/><br/>
</div>
<div class="null-line">
<span>Тема работы</span> 
<input type="text" class="required" size="25" value="" name="name-theme" id="name-theme" placeholder="Физика"/><br/>

</div>
    <div class="null-line">
    <input type="text" class="required" minlength="1" size="25" value="" name="number-pages" id="number-pages" placeholder="Кол-во страниц"/><br/>
	<input type="text" class="term-2 e-select-date" minlength="1" size="25" value="" name="yourname" id="yourname" placeholder="Срок сдачи"/><br/>
	</div>
	<div class="null-line">
	<span>Ваше имя</span><input type="text" class="required" minlength="1" size="25" value="" name="yourName" id="yourName" placeholder="Иван"/><br/>
	</div>
	<div class="null-line">
	<span>Электронная почта</span> <input type="text" class="required" minlength="1" size="25" value="" name="yourmail" id="yourmail" placeholder="ivanov@yahoo.com"/><br/>
	</div>
	<div class="null-line">
	<span>Телефон</span> <input type="tel" id="phone" class="phone-mask required" size="25" value="" name="phone" placeholder="+7(___) ___-____"/><br/>
<script type="text/javascript">
   jQuery(function($){
   $("#phone, .phone-mask").mask("+7(999) 999-9999");
   });
</script>
	</div>
	
	<div class="null-line">
	<span>Комментарий</span>
	<textarea rows="10" cols="50" name="msg" id="inputMsg"></textarea>
	</div>
	<div class="clearfix"></div>
	<div class="file_upload">
		<label for="inputFile" class="uploadButton"><p>Прикрепить материал</p><img src="image/clip.png" width="20px;"></img></label>
		<input style="opacity: 0; cursor:pointer;" type="file" name="inputFile[]" id="inputFile" multiple="true">
		<div class="successUpld"></div>
		<div class="countUpld"></div>
		<input type="hidden" id="typeForm" value="order" name="typeForm" />
		<button type="submit" name="insFeed" value="insFeed" class="cost event-sendform-cost">Узнать стоимость</button>
	</div>
</form>
</div>
			</div>
		</div>

	</div>

		<script>
		$(document).ready(function(){
			$(".e-select-date").datepicker();
			$(".e-select-date-main" ).datepicker({
			  buttonText: "Choose"
			});

			$('input[name="inputFile"]').change(function(){
				console.log($(this).val());
				if($(this).val()!=""){
					$('label[for="inputFile"]').addClass('active');
					$('label[for="inputFile"] p').html("Файл добавлен");
				}else{
					$('label[for="inputFile"]').addClass('active');
					$('label[for="inputFile"] p').html("Прикрепить материал");
				}
			});
			$('#callback').click(function() {
			$('.modal-window').fadeIn(function() {
			$('.window-container').addClass('visible');
			});
		});
			$('.close, .modal-window').click(function() {
			$('.modal-window').fadeOut().end().find('.window-container').removeClass('visible');
		});
			$('.window-container').click(function(event) {
				event.stopPropagation()
			});
		});
	</script>

	<script>
		$(document).ready(function() {
			$('#callback-1').click(function() {
			$('.modal-window').fadeIn(function() {
			$('.window-container').addClass('visible');
			});
		});
			$('.close, .modal-window').click(function() {
			$('.modal-window').fadeOut().end().find('.window-container').removeClass('visible');
		});
			$('.window-container').click(function(event) {
				event.stopPropagation()
			});
		});
	</script>

	<noindex><script async src="data:text/javascript;charset=utf-8;base64,ZnVuY3Rpb24gbG9hZHNjcmlwdChlLHQpe3ZhciBuPWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoInNjcmlwdCIpO24uc3JjPSIvL2xwdHJhY2tlci5ydS9hcGkvIitlO24ub25yZWFkeXN0YXRlY2hhbmdlPXQ7bi5vbmxvYWQ9dDtkb2N1bWVudC5oZWFkLmFwcGVuZENoaWxkKG4pO3JldHVybiAxfXZhciBpbml0X2xzdGF0cz1mdW5jdGlvbigpe2xzdGF0cy5zaXRlX2lkPTE2Mzg2O2xzdGF0cy5yZWZlcmVyKCl9O3ZhciBqcXVlcnlfbHN0YXRzPWZ1bmN0aW9uKCl7alFzdGF0Lm5vQ29uZmxpY3QoKTtsb2Fkc2NyaXB0KCJzdGF0c19hdXRvLmpzIixpbml0X2xzdGF0cyl9O2xvYWRzY3JpcHQoImpxdWVyeS0xLjEwLjIubWluLmpzIixqcXVlcnlfbHN0YXRzKQ=="></script></noindex>

</body>
</html>