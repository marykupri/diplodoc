<?
$this->load->helper('url');
$this->load->helper('form');?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
  	<!-- <link rel="stylesheet" href="/css/admin.css" type="text/css" /> --><!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
  	
<title></title>
</head>
<body>
<div class="container">
<div class="row">
<div class="col-md-4 col-md-offset-4" id="flogin">
	<form method="post" action="/admin/login" />
		<div class="form-group">
			<h2>Авторизация</h2>
			<label>Ваш логин:</label>
			<input type="text" id="login" name="login" class="form-control"/>
		</div>
		<div class="form-group">
			<label>Ваш пароль:</label>
			<input type="password" id="password" class="form-control" name="password"/> 
		</div>
		<div class="form-group">
			<input name="submit" type="submit" value="Вход" class="btn btn-default" />
		</div>
	</form>
</div>
</div>
</div>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

</body>
</html>