<div id="editForm">
  <form class="form-horizontal" method="post" action="/admin/typework">
<input type="hidden" name="inputId" value="<?php print $editWork['idstr'];?>">

    <div class="form-group" styel="display:block">
      <label for="inputName" class="col-sm-2 control-label">Название</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="inputName" name="inputName" value="<?php print $editWork['nazv'];?>">
      </div>
    </div>

    <div class="form-group" styel="display:block">
      <label for="inputParent" class="col-sm-2 control-label">Подгруппа</label>
      <div class="col-sm-10">
        <select name="inputParent" class="form-control">
          <option value='0'></option>
          <?foreach ($listGroup as $key => $value) {
            $select='';
            if($editWork['group']==$value['id']) $select=' selected';
            print '<option value="'.$value['id'].'"'.$select.'>'.$value['name'].'</option>';
          }?>
        </select>
      </div>
    </div>

    <div class="form-group" styel="display:block">
      <label for="inputCena" class="col-sm-2 control-label">Цена</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="inputCena" name="inputCena" value="<?php print $editWork['cena'];?>">
      </div>
    </div>

    <div class="form-group" styel="display:block">
      <label for="inputOriginal" class="col-sm-2 control-label">Оригинальность</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="inputOriginal" name="inputOriginal" value="<?php print $editWork['origin'];?>">
      </div>
    </div>

    <div class="form-group" styel="display:block">
      <label for="inputOriginal" class="col-sm-2 control-label">Ранг</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="inputRang" name="inputRang" value="<?php print $editWork['rang'];?>">
      </div>
    </div>

    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-default" value="sbmUpd" name="sbmUpd">Сохранить</button>
      </div>
    </div>
  </form>
</div>