<section id="textContainer">
	<div id="pageText">

<h2 class="contact">Контакты</h2>
<div class="info-area">

<div id="about">		
			<div id="map"></div>
			
 <script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
			<script>

			ymaps.ready(init);
        
function init() {
    var myMap = new ymaps.Map('map', {
            center: [55.804995, 37.585395],
            zoom: 16,
            controls: []
        }),  
    myPlacemark = new ymaps.Placemark([55.804995, 37.585395], {
          
        });

    myMap.geoObjects.add(myPlacemark);


}
			</script>

</div>
<div id="text-contacts" class="container" style="margin-top:20px; padding:0;">

			<img src="/image/office-hello.jpg" class="office-image" style="width:50%; float:left; margin-right:20px; margin-bottom:20px;"/>

				<p class="address">
				<b style="font-size:24px; font-weight:bold; display:block; ">Наши контактные данные</b>
				г. Москва, ул. Б. Новодмитровская, 36<br/> <a href="mailto:hello@hellodiplom.ru" class="mail-link" target="_blank">HELLO@HELLODIPLOM.RU</a><br/>
				Федеральный телефон: <a class="telephone-contacts" href="tel:88005554658">8 (800) 555-46-58</a><br/>
				Московский телефон: <a class="telephone-contacts" href="tel:+74997059549">+7 (499) 705-95-49</a>
			</div>

</div>
			

</section>