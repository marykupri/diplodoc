<?php
$optnListType='';

if(!empty($listType))
    foreach ($listType as $key => $value) {
      $optnListType.= "<option value='$value->id'>$value->nazv</option>";
    }

?><div id="editForm">
  <form role="form" method="post" action="feedback" enctype="multipart/form-data" >
    <div class="form-group">
      <label for="exampleInputEmail1">Имя Фамилия</label>
      <input type="text" class="form-control" id="exampleInputEmail1" name="inputName">
    </div>
    <div class="form-group">
      <label for="exampleInputPhone">Телефон</label>
      <input type="text" class="form-control" id="exampleInputPhone" name="inputPhone">
    </div>
    <div class="form-group">
      <label for="exampleInputPassword1">Email</label>
      <input type="email" class="form-control" id="exampleInputPassword1" name="inputEmail">
    </div>

    <div class="form-group">
      <label for="exampleInputType" class="col-sm-2 control-label">Тип работы</label>
        <select  class="form-control" id="exampleInputType" name="inputType">
          <option value="0">Выберите тип работы</option>
          <?php print $optnListType;?>
        </select>
    </div>  

     <div class="form-group">
      <label for="exampleInputTheme">Тема</label>
      <input type="text" class="form-control" id="exampleInputTheme" name="inputTheme">
    </div>

        <div class="form-group">
          <label for="exampleInputFile" class="col-sm-2 control-label">Прикрепить файл</label>
          <input type="file" class="form-control" id="exampleInputFile" name="inputFile">
        </div> 

    <button type="submit" class="btn btn-default" name="insFeed" value="insFeed">Отправить</button>
  </form>
</div>