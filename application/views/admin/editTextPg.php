<?
  $this->load->helper('url');
  $bsurl=base_url();

  $namestr='';
  $descrstr='';
  $idstr='';


  if(!empty($get_textPage))
    foreach ($get_textPage as $key => $value) {
      $namestr=$value->name;
      $descrstr=$value->desc;
      $idstr=$value->id;
    }

?>

<div id="editForm">
  <form class="form-horizontal" method="post" action="/admin/textpage">
<input type="hidden" name="inputId" value="<?php print $idstr;?>">
<!--     <div class="form-group" styel="display:block">
      <label for="inputEmail3" class="col-sm-2 control-label">Название</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="inputEmail3" name="inputName" value="<?php //print $namestr;?>">
      </div>
    </div> -->
    <div class="form-group">
      <label for="inputPassword3" class="col-sm-2 control-label">Описание</label>
      <div class="col-sm-10">
        <textarea id="inputComm" class="form-control" rows="50" name="inputComm"><?php print $descrstr;?></textarea>
      </div>
    </div>

    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-default" value="sbmUpd" name="sbmUpd">Сохранить</button>
      </div>
    </div>
  </form>
</div>

<script>
CKEDITOR.replace( 'inputComm', {
uiColor: '#dddddd',
toolbar: [
['Source'] ,
['Cut','Copy','Paste','PasteText','PasteFromWord'] ,
['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
//['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'], 
['BidiLtr', 'BidiRtl'],
['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
['NumberedList','BulletedList','-','Outdent','Indent','Blockquote','CreateDiv'],
['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
['Link','Unlink','Anchor'],
['Image','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],
['Styles','Format','FontSize'],
['TextColor','BGColor','Maximize', 'ShowBlocks']

]
} );
</script> 