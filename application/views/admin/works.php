<?
	$tdListWorks = '';
	
	$styleModal = '';
	$classModal= '';


	if(!empty($listWork)){
		foreach ($listWork as $key => $value) {
			if($value['inShop'] == 1)
				$value['inShop']='В магазине';
			$tdListWorks.="<tr>
							<td>".$value['id']."</td>
							<td>".$value['date']."</td>
							<td style='width:500px;'><a href='".$value['link']."' target='_blank'>".$value['name']."</a></td>
							<td>".$value['inShop']."</td>
						   	<td>".$value['cena']."</td><td>".$value['typew']."</td>
						   	<td>".$value['nazv']."</td><td>".$value['authorw']."</td>
			               	<td><a href='/admin/editWork/".$value['id']."'>редактировать</a><br>
			               	<a href='/admin/deltWork/".$value['id']."'>удалить</a></td></tr>";
		}
	}
	//<td>".substr($value->descr , 0 , 100)."</td><td>$value->tags</td>


?>

<div class="addWork">
<button type="button" class="btn btn-default"  data-toggle="modal" data-target="#myModal">Добавить работу</button>  
</div>
<div class="search">
<form action="/admin" method="post">
<input type="text" class="form-control" name="search" placeholder="Поиск" value="<?if (isset($srchQuery['quest'])) print $srchQuery['quest'];?>">
<input type="checkbox" id="tags" name="tags" <?if (isset($srchQuery['tagsrch'])) print 'checked';?>><label for="tags">по тэгам</label>
<input type="checkbox" id="name" name="name" <?if (isset($srchQuery['namesrch'])) print 'checked';?>><label for="name">по названию</label>
<input type="checkbox" id="descr" name="descr" <?if (isset($srchQuery['descrsrch'])) print 'checked';?>><label for="descr">по описанию</label>
<input type="checkbox" id="number" name="number" <?if (isset($srchQuery['numbersrch'])) print 'checked';?>><label for="number">по номеру заказа</label>
<input type="hidden" class="form-control" name="srchBtn" value="srchBtn">
</form>
</div>

<table class="table table-hover">
  <tr><th>#</th>
  	<th><a href="/admin<?print $sort['data']['url'];?>">Дата<span class="glyphicon <?print $sort['data']['pict'];?>"></span></a></th>
  	<th><a href="/admin<?print $sort['name']['url'];?>">Название работы<span class="glyphicon <?print $sort['name']['pict'];?>"></span></a></th>
  	<th>Наличие</th>
  	<th><a href="/admin<?print $sort['cena']['url'];?>">Цена<span class="glyphicon <?print $sort['cena']['pict'];?>"></span></a></th>
  	<th><a href="/admin<?print $sort['type']['url'];?>">Тип работы<span class="glyphicon <?print $sort['type']['pict'];?>"></span></a></th>
  	<th><a href="/admin<?print $sort['razdel']['url'];?>">Раздел<span class="glyphicon <?print $sort['razdel']['pict'];?>"></span></a></th>
  	<th><a href="/admin<?print $sort['author']['url'];?>">Автор<span class="glyphicon <?print $sort['author']['pict'];?>"></span></a></th>
  	<th>Действия</th></tr>
  <?php print $tdListWorks;?>
</table>

<!-- <th>Описание работы</th><th>Теги</th>

<!-- Modal -->
<div class="modal fade<?php print $classModal;?>" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"<?php print $styleModal;?>>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Добавление новой работы</h4>
      </div>

		<form method="post" enctype="multipart/form-data" action="/admin">
		  <div class="modal-body">
			  <div class="form-group">
			    <label for="exampleInputFile">Загрузить файл</label>
			    <input type="file" id="exampleInputFile" name="inputFile">
			    <!-- <p class="help-block">Example block-level help text here.</p> -->
			  </div>      	
		      <div class="form-group">
			    <label for="exampleInputName">Название работы</label>
			    <input type="text" class="form-control" id="exampleInputName" name="inputName">
			  </div>
			  <div class="form-group">
			    <label for="exampleInputDescr">Описание работы</label>
			    <textarea class="form-control" id="exampleInputDescr" name="inputDescr"></textarea>
			  </div>
			  <div class="form-group">
			    <label for="exampleInputType">Тип работы</label>
			    <select class="form-control" id="exampleInputType" name="inputType">
			    	<option value="0">Выберите тип работы</option>
			    	<?php print $optnListType;?>
			    </select>
			  </div>
			  <div class="form-group">
			    <label for="exampleInputRazdel">Раздел</label>
			    <select class="form-control" id="exampleInputRazdel" name="inputRazdel">
			    	<option value="0">Выберите раздел</option>
			    	<?php print $optnListSctn;?>
			    </select>
			  </div>
			  <div class="form-group">
			    <label for="exampleInputAuthor">Автор</label>
			    <select class="form-control" id="exampleInputAuthor" name="inputAuthor">
			    	<option value="0">Выберите автора</option>
			    	<?php print $optnListAuthor;?>
			    </select>
			  </div>
			  <div class="form-group">
			    <label for="exampleInputPassword1">Теги</label>
			    <input type="text" class="form-control" id="exampleInputPassword1" name="inputPass">
			  </div>
			  <div class="form-group">
			    <label for="exampleInputCena">Цена</label>
			    <input type="text" class="form-control" id="exampleInputCena" name="inputCena">
			  </div>
		  </div>

		  <div class="modal-footer">
		    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
		    <button type="submit" class="btn btn-primary" name="addWork" value="addWork">Сохранить изменения</button>
		  </div>
		<form>

    </div>
  </div>
</div>