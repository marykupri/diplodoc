<div id="editForm">
  <form class="form-horizontal" method="post" action="/admin/manager">
<input type="hidden" name="inputId" value="<?php print $dataManager['id'];?>">

    <div class="form-group" styel="display:block">
      <label for="inputFam" class="col-sm-2 control-label">Фамилия</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="inputFam" name="inputFam" value="<?php print $dataManager['surname'];?>">
      </div>
    </div>

    <div class="form-group" styel="display:block">
      <label for="inputName" class="col-sm-2 control-label">Имя</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="inputName" name="inputName" value="<?php print $dataManager['name'];?>">
      </div>
    </div>

    <div class="form-group" styel="display:block">
      <label for="inputPhone" class="col-sm-2 control-label">Телефон</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="inputPhone" name="inputPhone" value="<?php print $dataManager['phone'];?>">
      </div>
    </div>

    <div class="form-group" styel="display:block">
      <label for="inputEmail" class="col-sm-2 control-label">Email</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="inputEmail" name="inputEmail" value="<?php print $dataManager['email'];?>">
      </div>
    </div>

    <div class="form-group" styel="display:block">
      <label for="inputComm" class="col-sm-2 control-label">Комментарий</label>
      <div class="col-sm-10">
        <textarea class="form-control" rows="10" id="inputComm" name="inputComm"><?php print $dataManager['comment'];?></textarea>
      </div>
    </div>

     <div class="form-group" styel="display:block">
      <label for="inputDelete" class="col-sm-2 control-label">Удалить</label>
      <div class="col-sm-10">
        <input type="checkbox" id="inputDelete" name="inputDelete" value="1">
      </div>
    </div>   

    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-default" value="sbmUpd" name="sbmUpd">Сохранить</button>
      </div>
    </div>
  </form>
</div>