<div id="editForm">
  <form class="form-horizontal" method="post" action="/admin/applicat">
<input type="hidden" name="inputId" value="<?php print $listFeed['id'];?>">

<h3>Прикрепленные материалы</h3>

        <?if(isset($orderdoc)&&!empty($orderdoc)){
          foreach ($orderdoc as $key => $value) {
            print '<div class="editWorkDoc">
            <a href="/completedWork/orderDoc/'.$value['file'].'">
            <img src="/image/doc1.png"><br>'.$value['nazv'].'
            </a></div>';
          }          
        }
        // <a href="/admin/delFileWork/'.$work['id'].'/'.$value['iddoc'].'">Удалить</a><br>
        ?>

<hr>
    <div class="form-group">
      <label for="inputEmail3" class="col-sm-2 control-label">Тема работы</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="inputEmail3" name="inputName" value="<?php print $listFeed['themeuser'];?>">
      </div>
    </div>
        <div class="form-group">
      <label for="exampleInputType" class="col-sm-2 control-label">Тип работы</label>
      <div class="col-sm-10">
        <select  class="form-control" id="exampleInputType" name="inputType">
          <option value="0">Выберите тип работы</option>
          <?php print $optnListType;?>
        </select>
      </div>
    </div> 
    <div class="form-group">
      <label for="inputPassword3" class="col-sm-2 control-label">Комментарий</label>
      <div class="col-sm-10">
        <textarea class="form-control" id="inputPassword3" rows="10" name="inputComm"><?php print $listFeed['comment'];?></textarea>
      </div>
    </div>

    <div class="form-group">
      <label for="exampleInputStatus" class="col-sm-2 control-label">Статус</label>
      <div class="col-sm-10">
        <select  class="form-control" id="exampleInputStatus" name="inputStatus">
          <option value="0">Выберите статус</option>
          <?php print $optnListStat;?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="exampleInputManager" class="col-sm-2 control-label">Менеджер</label>
      <div class="col-sm-10">
        <select  class="form-control" id="exampleInputManager" name="inputManager">
          <option value="0">Выберите менеджера</option>
          <?php print $optnListManager;?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="exampleInputAuthor" class="col-sm-2 control-label">Автор</label>
      <div class="col-sm-10">
        <select  class="form-control" id="exampleInputAuthor" name="inputAuthor">
          <option value="0">Выберите автора</option>
          <?php print $optnListAuthor;?>
        </select>
      </div>
    </div>

  <hr>

    <div class="form-group">
      <label for="exampleInputNamekl" class="col-sm-2 control-label">Имя клиента</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="exampleInputNamekl" name="inputNamekl" value="<?php print $listFeed['nameuser'];?>">
      </div>
    </div>

    <div class="form-group">
      <label for="exampleInputPhone" class="col-sm-2 control-label">Телефон</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="exampleInputPhone" name="inputPhone" value="<?php print $listFeed['phoneuser'];?>">
      </div>
    </div>

    <div class="form-group">
      <label for="exampleInputEmail" class="col-sm-2 control-label">Email</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="exampleInputEmail" name="inputEmail" value="<?php print $listFeed['emailuser'];?>">
      </div>
    </div>

    <div class="form-group">
      <label for="exampleInputCameFrom" class="col-sm-2 control-label">Откуда пришел</label>
      <div class="col-sm-10">
        <select  class="form-control" id="exampleInputCameFrom" name="inputCameFrom">
          <option value="0"></option>
          <?php print $optnListCameFrom;?>
        </select>
      </div>
    </div>

  <hr>

    <div class="form-group">
      <label for="exampleInputPrice" class="col-sm-2 control-label">Цена</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="exampleInputPrice" name="inputPrice" value="<?php print $listFeed['price'];?>">
      </div>
    </div>

    <div class="form-group">
      <label for="exampleInputPrepay" class="col-sm-2 control-label">Предоплата</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="exampleInputPrepay" name="inputPrepay" value="<?php print $priceFeed['prepay']['pay'];?>">
        <?if(!empty($priceFeed['prepay']['data'])){
          print '<span id="greenPay">Заказ оплачен на сумму '.$priceFeed['prepay']['pay'].' рублей '.$priceFeed['prepay']['data'].'</span>';
        }else{
          print '<span class="linkSendMail" onclick="sendPay('.$priceFeed['prepay']['pay'].','.$priceFeed['prepay']['id'].',\''.$listFeed['emailuser'].'\',\'prepay\')">Отправить письмо клиенту</span>';
          print '<div id="msgPrepay"></div>';
        }?>
        
      </div>
    </div>

    <div class="form-group">
      <label for="exampleInputRest" class="col-sm-2 control-label">Остаток</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="exampleInputRest" name="inputRest" value="<?php print $priceFeed['remain']['pay'];?>">
        <?if(!empty($priceFeed['remain']['data'])){
          print '<span id="greenPay">Заказ оплачен на сумму '.$priceFeed['remain']['pay'].' рублей '.$priceFeed['remain']['data'].'</span>';
        }else{
          print '<span class="linkSendMail" onclick="sendPay('.$priceFeed['remain']['pay'].','.$priceFeed['remain']['id'].',\''.$listFeed['emailuser'].'\',\'remain\')">
          Отправить письмо клиенту</span>';
        }?>        
        <div id="msgRemaind"></div>
      </div>
    </div>

  <hr>

  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <div class="checkbox">
        <label>
          <input type="checkbox" name="inputDlt"> Удалить
        </label>
      </div>
    </div>
  </div>

    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-default" value="sbmUpd" name="sbmUpd">Сохранить изменения</button>
      </div>
    </div>
  </form>
</div>