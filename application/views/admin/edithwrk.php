<div id="editForm">
  <form class="form-horizontal" enctype="multipart/form-data" method="post" action="/admin/editWork/<?php print $work['id'];?>">
    <h3>Готовая работа</h3>
    <div class="form-group">
      <label for="exampleInputFile" class="col-sm-2 control-label">Выполненная работа</label>
        <div class="col-sm-10">
          <?php if(!empty($work['link'])) 
          print '<a href="/'.$work['link'].'"><img src="/image/doc1.png"></a>
          <input type="checkbox" name="inputFileDel">Удалить';
          else 
          print '<input type="file" class="form-control" id="exampleInputFile" name="inputFile">'?>
        </div>
        
    </div>
    <div class="form-group">
      <label for="exampleSuppFile" class="col-sm-2 control-label">Сопроводительные документы</label>
      <div class="col-sm-10">
        <?if(isset($docSupp)&&!empty($docSupp)){
          foreach ($docSupp as $key => $value) {
            print '<a href="/completedWork/suppDoc/'.$value['filedoc'].'">
            <img src="/image/doc1.png">'.$value['nazvdoc'].'
            </a><input type="checkbox" name="inputSuppFileDel" value="'.$value['iddoc'].'">Удалить';
          }          
        }else{
          print '<input type="file" class="form-control" id="exampleSuppFile" name="suppFile">';
        }?>
      </div>
    </div> 
    <hr>
    <h3>Внуренние материалы</h3>

        <?if(isset($docAddit)&&!empty($docAddit)){
          foreach ($docAddit as $key => $value) {
            print '<div class="editWorkDoc"><a href="/admin/delFileWork/'.$work['id'].'/'.$value['iddoc'].'">Удалить</a><br>
            <a href="/completedWork/suppDoc/'.$value['filedoc'].'">
            <img src="/image/doc1.png"><br>'.$value['nazvdoc'].'
            </a></div>';
          }          
        }?>
      
      <input type="file" class="form-control" id="exampleInputFile" name="inputAddit[]" multiple="multiple">

    <hr>
    <div class="form-group">
      <label for="inputEmail3" class="col-sm-2 control-label">Название работы</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="inputEmail3" name="inputName" value="<?php print $work['name'];?>">
      </div>
    </div>
    <div class="form-group">
      <label for="inputPassword3" class="col-sm-2 control-label">Описание работы</label>
      <div class="col-sm-10">
        <textarea class="form-control" id="inputPassword3" rows="15" name="inputDescr"><?php print $work['descr'];?></textarea>
      </div>
    </div>

    <div class="form-group">
      <label for="exampleInputType" class="col-sm-2 control-label">Тип работы</label>
      <div class="col-sm-10">
        <select  class="form-control" id="exampleInputType" name="inputType">
          <option value="0">Выберите тип работы</option>
          <?php print $optnListType;?>
        </select>
      </div>
    </div>    

    <div class="form-group">
      <label for="exampleInputRazdel" class="col-sm-2 control-label">Раздел</label>
      <div class="col-sm-10">
        <select  class="form-control" id="exampleInputRazdel" name="inputRazdel">
          <option value="0">Выберите раздел</option>
          <?php print $optnListSctn;?>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label for="exampleInputAuthor" class="col-sm-2 control-label">Автор</label>
      <div class="col-sm-10">
        <select  class="form-control" id="exampleInputAuthor" name="inputAuthor">
          <option value="0">Выберите автора</option>
          <?php print $optnListAuthor;?>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label for="inputTags" class="col-sm-2 control-label">Теги</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="inputTags" name="inputTags" value="<?php print $work['tags'];?>">
      </div>
    </div>

    <div class="form-group">
      <label for="inputCena" class="col-sm-2 control-label">Цена</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="inputCena" name="inputCena" value="<?php print $work['cena'];?>">
      </div>
    </div>

    <div class="form-group">
      <label for="inputShop" class="col-sm-2 control-label"></label>
      <div class="col-sm-10">
        <input type="checkbox" id="inputShop" name="inputShop" value='1' <?if($work['inShop'] == 1) print ' checked';?>> Показывать в магазине
      </div>
    </div>
  
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <div class="checkbox">
          <label>
            <input type="checkbox"> Удалить
          </label>
        </div>
      </div>
    </div>

    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-default" value="sbmUpd" name="sbmUpd">Сохранить</button>
      </div>
    </div>
  </form>
</div>