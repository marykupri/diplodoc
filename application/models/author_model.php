<?php
class Author_model extends CI_Model{

	function get_listAuthor($id=''){
		$usl='';
		if(!empty($id))
			$usl=' where id='.$id;
		$sql="select * from authors".$usl;
		$authors = $this->db->query($sql);
		$listAuthors = array();

		foreach ($authors->result_array() as $row)
			if(!empty($id))
				$listAuthors=$row;
			else
				$listAuthors[]=$row;

		return $listAuthors;
	}

	function add_author($famil,$name,$phone,$email,$comment){
		if(empty($name)) return;
		$sql="INSERT INTO `authors`(`fam`, `name`, `phone`, `email`, `comment`) 
					 VALUES ('$famil','$name','$phone','$email','$comment')";
		$this->db->query($sql);
	}

	function edit_author($id,$famil,$name,$phone,$email,$comment){
		if(empty($id)) return;
		$sql="update `authors` SET `fam`='$famil',`name`='$name',`phone`='$phone',`email`='$email',`comment`='$comment' WHERE `id`=$id";
		$this->db->query($sql);
	}

	function dlt_author($id){
		if(empty($id)) return;
		$sql="DELETE FROM `authors` WHERE id=$id";
		$this->db->query($sql);
	}
}