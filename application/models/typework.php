<?php
class Typework extends CI_Model{

	function getEditTypeWork($id){
		if(empty($id)) return;

		$sql="SELECT * FROM `type_work` where id=$id";
		$query = $this->db->query($sql);
		return $query->result();
	}

	function editListTypeWork($id,$name,$group,$cena,$origin,$rang){
		$sql = "UPDATE `type_work` SET `nazv`='".$name."', parent=$group, `cena`='".$cena."', `origin`='$origin', `rang`=$rang WHERE id = $id";
		$this->db->query($sql);		
	}

	function addTypeWork($name,$group,$cena,$origin){
		if(empty($group)) $group='""';
		$sql="insert INTO `type_work`(`nazv`,`parent`, `cena`, `origin`) VALUES 
		('$name',$group,'$cena','$origin')";
		$this->db->query($sql);
	}

	function dltTypeWork($id){
		$sql="delete from type_work where id=$id";
		$this->db->query($sql);
	}

	function getListTypeWork(){
		$sql="select tw.*,gtw.name_group from type_work tw left join groupTypeWork gtw on tw.parent=gtw.id order by rang";
		$query = $this->db->query($sql);
		return $query->result();
	}

	function getGroupTypeWork(){
		$queryGroup=array();
		$sql="select * from groupTypeWork order by id";
		$query = $this->db->query($sql);
		return $query->result();
	}

	function get_TypeWork(){
		$sql="select * from type_work order by rang";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			$arrType=array();
			foreach ($query->result_array() as $row) {
				$arrType[]=$row;
			}

		$sql="select * from groupTypeWork";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			$arrGroup=array();
			foreach ($query->result_array() as $row) {
				$arrGroup[$row['id']]=$row['name_group'];
			}
		}

	$array = array();	

		foreach ($arrType as $key => $value) {
			$arrTemp=array();
			if($value['parent'] == 0){				
				$arrTemp['id']=$value['id'];
				$arrTemp['name']=$value['nazv'];
				$arrTemp['group']='0';
				$arrTemp['arr']='';
				$array[]=$arrTemp;
			}else{
				$a=array();
				foreach ($array as $arr => $val) {
					$a[$arr]=$val['group'];
				}

				if(in_array($value['parent'], $a)){
					foreach ($a as $arr => $valarr) {
						if($valarr == $value['parent']){
							$arrTemp['id']=$value['id'];
							$arrTemp['name']=$value['nazv'];
							$arrTemp['group']='0';
							$arrTemp['arr']='';
							$array[$arr]['arr'][]=$arrTemp;
						}							
					}					
				}else{
					$arrTemp['id']='0';
					$arrTemp['name']=$arrGroup[$value['parent']];
					$arrTemp['group']=$value['parent'];
					$arrTemp['arr']='';
					$array[]=$arrTemp;					
				}
			}		
		}


			return $array;
		}				
	}

	function get_data_type($id){
		if(empty($id)) return;
		$sql="select cena, origin from type_work where id=$id";
		$query = $this->db->query($sql);
		if($query->num_rows()>0){
			$arr=array();
			foreach ($query->result_array() as $row) {
				$arr=$row;
			}
		}
		return $arr;

	}
}