<?php
class Handiwork extends CI_Model{

	function getListWorks($id='',$sort='',$order='',$srchQuery=''){
		$usl='';
		$uslOrd='';
		if(!empty($id)) $usl = " where id=$id";

		if(!empty($sort)&&!empty($order)){ 
			switch ($sort) {
				case 'name':
					$sort ="hw.name";
					break;
				case 'data':
					$sort="hw.date";
					break;
				case 'cena':
					$sort="hw.cena";
					break;
				case 'type':
					$sort="typew";
					break;
				case 'author':
					$sort="authorw";
					break;
				case 'razdel':
					$sort="nazv";
					break;
				default:
					$sort="hw.id";
			}
			$uslOrd=" order by $sort $order";
		}
		if(!empty($srchQuery)){
			if(!empty($srchQuery['tagsrch'])){
				$usl.="tags like '%".$srchQuery['quest']."%'";
			}
			if(!empty($srchQuery['namesrch'])){
				if(!empty($usl)) $usl.=' or ';
				$usl.="name like '%".$srchQuery['quest']."%'";
			}
			if(!empty($srchQuery['descrsrch'])){
				if(!empty($usl)) $usl.=' or ';
				$usl.="descr like '%".$srchQuery['quest']."%'";
			}
			if(!empty($srchQuery['numbersrch'])){
				if(!empty($usl)) $usl.=' or ';
				$usl.="id = '".$srchQuery['quest']."'";
			}
		if(!empty($usl))
			$usl=' where '.$usl;
		}

		$sql="select hw.*,(select sw.nazv from sectionWork sw where sw.id=hw.razdel) nazv,
		(select tw.nazv from type_work tw where tw.id=hw.type) typew,
		(select a.name from authors a where a.id=hw.author) authorw from handiwork hw".$usl.$uslOrd;
		
		$query = $this->db->query($sql);
		$array=array();
		foreach ($query->result_array() as $row) {
			if(!empty($id))
				$array=$row;
			else
				$array[]=$row;
		}
		return $array;
	}
	function addWork($name,$descr,$cena,$tags,$type,$author,$razdel,$uploadfile){
		$sql="insert INTO `handiwork`(`name`, `date`, `descr`,`cena`,`type`, `razdel`,`author`, `tags`,`link`) 
				VALUES ('$name',now(),'$descr',$cena,$type,$razdel,$author,'$tags','$uploadfile')";
		$this->db->query($sql);
	}

	function updWork($id,$name,$descr,$type,$author,$tags,$razdel,$cena,$inShop){
		if(empty($inShop)) $inShop="null";
		$sql = "UPDATE `handiwork` SET `name`='$name',`descr`='$descr',
				`cena`=$cena,`razdel`=$razdel,
				`type`=$type,`author`=$author,
				`tags`='$tags',`inShop`=$inShop 
				WHERE id = $id";
		$this->db->query($sql);
	}

	function addFile($id,$arrFile){
		if(!empty($arrFile)){
			$sql="UPDATE `handiwork` SET `link`='completedWork/".$arrFile['file']."' WHERE id=$id";
			$this->db->query($sql);
		}
	}

	function addDoc($id,$arrFile){
		$iddoc='';
		if(!empty($arrFile)){
			$sql="INSERT INTO `listdoc`(`nazvdoc`, `filedoc`, `typedoc`) 
			VALUES ('".$arrFile['name']."','".$arrFile['file']."','".$arrFile['type']."')";
			$this->db->query($sql);
			$iddoc=mysql_insert_id();
		}
		if(!empty($iddoc)&&!empty($id)){
			$sql="INSERT INTO `listdocwork`(`idwork`, `iddoc`) VALUES ($id,$iddoc)";
			$this->db->query($sql);
		}
	}

	function addOrderDoc($id,$arrFile){
		$iddoc='';
		if(!empty($arrFile)){
			for ($i=0; $i < count($arrFile) ; $i++) {
				$sql="INSERT INTO `feeddoc`(`nazv`, `file`) VALUES ('".$arrFile[$i]['name']."','".$arrFile[$i]['file']."')";
				$this->db->query($sql);
				$iddoc=mysql_insert_id();

				if(!empty($iddoc)&&!empty($id)){
					$sql="INSERT INTO `feeddocord`(`idord`, `iddoc`) VALUES ($id,$iddoc)";
					$this->db->query($sql);
				}
			}
		}
	}

	function addDocMulti($id,$arrFile){
		if(!empty($arrFile)){
			for ($i=0; $i < count($arrFile) ; $i++) { 
				$this->addDoc($id,$arrFile[$i]);
			}
		}
	}

	function getListDoc($id){
		$sql="select ldw.iddoc,ld.nazvdoc,ld.filedoc,ld.typedoc 
		from listdocwork ldw 
		left join listdoc ld on ldw.iddoc=ld.id
		where ldw.idwork=$id";
		$query=$this->db->query($sql);
		$array=array();
		foreach ($query->result_array() as $row)
				$array[]=$row;
		return $array;
	}

	function getListDocOrd($id){
		$sql="select ldw.idord,ld.nazv,ld.file 
		from feeddocord ldw 
		left join feeddoc ld on ldw.iddoc=ld.id
		where ldw.idord=$id";
		$query=$this->db->query($sql);
		$array=array();
		foreach ($query->result_array() as $row)
				$array[]=$row;
		return $array;
	}

	function deleteWork($id){
		if(empty($id)) return;
		$sql="DELETE FROM `handiwork` WHERE id=$id";
		$this->db->query($sql);
	}

	function delSuppFile($id,$suppDel){
		if(empty($id)||empty($suppDel)) return;
		$sql="DELETE FROM `listdocwork` WHERE idwork=$id and iddoc=$suppDel";
		$this->db->query($sql);
		$sql="DELETE FROM `listdoc` WHERE id=$suppDel";
		$this->db->query($sql);
	}

	function delFile($id){
		if(empty($id)) return;
		$sql="UPDATE `handiwork` SET `link`='' WHERE id=$id";
		$this->db->query($sql);
	}

	function getListSectn(){
		$sql="select * from sectionWork order by nazv";
		$query = $this->db->query($sql);
		return $query->result();
	}

	function addFeed($name,$phone,$email,$typeWrk,$specWrk,$theme,$inputMsg,$data){
		$id_dates = date('Y-m-d H:i:s');
		$sql="insert INTO `feedback`(`dateAdd`,`nameuser`, `phoneuser`, `emailuser`, `typework`, `specwork`, `themeuser`,`message`,`finedate`) VALUES 
		('$id_dates','$name','$phone','$email',$typeWrk,'$specWrk','$theme','$inputMsg','$data')";
		$this->db->query($sql);
		$iddoc=mysql_insert_id();
			if($iddoc)
				return $iddoc;
	}

	function getListStatus(){
		$sql="SELECT * FROM `statusAppl`";
		$query = $this->db->query($sql);
		return $query->result();
	}

	function getListSpecWork(){
		$sql="SELECT * FROM `sectionWork` order by nazv";
		$query = $this->db->query($sql);
		return $query->result();
	}

	function getFindWork($name='',$type='',$spec=''){
		$usl='';
		if(!empty($type)&&!empty($spec))
			$usl=" and type=$type and razdel=$spec";
		elseif(!empty($type))
			$usl=" and type=$type";
		elseif(!empty($spec))
			$usl=" and razdel=$spec";
		elseif(!empty($name))
			$usl=" and name like '%$name%'";

		$sql="select hw.id, hw.name, hw.link,hw.type, tw.nazv, hw.cena from handiwork hw left join type_work tw on tw.id=hw.type where inShop=1 ".$usl;
		$query = $this->db->query($sql);
		$arr=array();
		foreach ($query->result_array() as $row) {
			$arr[]=$row;
		}
		return $arr;
	}

	function getWorkById($id){
		$sql="select * from handiwork where id='".$id."' LIMIT 1";
		$query = $this->db->query($sql);
		return $query->result();
	}
}