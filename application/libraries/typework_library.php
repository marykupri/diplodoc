<?if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

  class Typework_library {
    public $typeWrk;

    public function __construct(){
    }

    function listType($idType='')
    {
       $CI = &get_instance();
       $CI->load->database();
       $CI->load->model('typework');

       $list = $CI->typework->get_TypeWork();

       $listOpt='';

        $listOpt.='<option value="0" rel="0" disabled selected>Выберите тип</option>';
        foreach ($list as $key => $value) {
          if(!empty($value['arr'])){
            $listOpt.='<optgroup class="color" label="'.$value['name'].'">';
            foreach ($value['arr'] as $temp => $val) {
              $select='';
              if(isset($idType)&&$val['id'] == $idType) $select=' selected';
              $listOpt.='<option class="color" value="'.$val['id'].'" rel2="'.$val['name'].'"'.$select.'>'.$val['name'].'</option>';
            }
            $listOpt.='</optgroup>';
          }else{
            $select='';
            if(isset($idType)&&$value['id'] == $idType) $select=' selected';
            $listOpt.='<option class="color" value="'.$value['id'].'" rel2="'.$value['name'].'"'.$select.'>'.$value['name'].'</option>';  
          }              
        }

        $this->typeWrk['list'] = $listOpt;

       return $CI->load->view('/listWorkSelect',$this->typeWrk,true);
    }
  }
?>