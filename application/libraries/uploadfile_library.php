<?if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

  class Uploadfile_library {
    //public $upload;
    public function __construct(){
    }

    function uploadWorkIsDone($file){
      var_dump($file);
    }

    function uploadFile($file,$uploaddir){
            $arrDoc=array();
            $FileFoto=trim(basename($file['name']));
            $FileFoto=mb_strtolower($FileFoto);
            $fileName=md5($FileFoto.time());

            $FileSize=basename($file['size']);

            $uploadfile='';
            

            $ext_type = array('pdf','doc');
            $temp = explode('.', $FileFoto);
            $ext = $temp[count($temp)-1];
            $ext=mb_strtolower($ext);

            if($FileSize>0){
                  $uploadfile = $uploaddir.$fileName.'.'.$ext;
                  if(!empty($uploadfile)){
                          if(file_exists($uploadfile)){
                              unlink($uploadfile);
                          }
                        }

                  if (!move_uploaded_file($file['tmp_name'], $uploadfile)){
                       $FileFoto='';
                       $uploadfile='';
                    }
             }
             else $FileFoto='';

             $arrDoc['name']=$file['name'];
             $arrDoc['file']=$fileName.'.'.$ext;
             $arrDoc['type']=1;

             return $arrDoc;
    }

    function uploadMulti($file,$uploaddir){

          if(!empty($file)){
            $arrDoc=array();
            for ($i=0; $i <count($file['name']) ; $i++) { 
           
            $FileFoto=trim(basename($file['name'][$i]));
            $FileFoto=mb_strtolower($FileFoto);
            $fileName=md5($FileFoto.time());

            $FileSize=basename($file['size'][$i]);

            $uploadfile='';

            $ext_type = array('pdf','doc');
            $temp = explode('.', $FileFoto);
            $ext = $temp[count($temp)-1];
            $ext=mb_strtolower($ext);

            if($FileSize>0){
                  $uploadfile = $uploaddir.$fileName.'.'.$ext;
                  if(!empty($uploadfile)){
                          if(file_exists($uploadfile)){
                              unlink($uploadfile);
                          }
                        }

                  if (!move_uploaded_file($file['tmp_name'][$i], $uploadfile)){
                       $FileFoto='';
                       $uploadfile='';
                    }

                  $arrDoc[$i]['file']=$fileName.'.'.$ext;
                  $arrDoc[$i]['name']=$file['name'][$i];
                  $arrDoc[$i]['type']=2;
             }
             else $FileFoto='';
            }
          }
             return $arrDoc;
    }
  }
?>