<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class getWork extends CI_Controller {

	public function index()
	{
		$this->load->model('handiwork');

		$id=(int)$this->input->get('id');

		if($id==0){
			$this->data['error']="Не выбрана работа. Перейдите в начало поиска";
		}else{

			$workinfo=$this->handiwork->getWorkById($this->input->get('id'));		

			$this->data['work_date']=$workinfo[0]->date;
			$this->data['work_name']=$workinfo[0]->name;
			$this->data['work_id']=$workinfo[0]->id;
			$this->data['work_description']=$workinfo[0]->descr;
			$this->data['work_price']=$workinfo[0]->cena;
			$this->data['work_type']=$workinfo[0]->type;
			$this->data['work_category']=$workinfo[0]->razdel;
			$this->data['work_tags']=$workinfo[0]->tags;

		}


		$this->dataloc['content'] = $this->load->view('work_getWork',$this->data, true);
		$this->load->view('main',$this->dataloc);
	}
}
