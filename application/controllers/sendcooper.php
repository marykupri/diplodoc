<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sendcooper extends CI_Controller {

	public function index()
	{
	$this->load->model('handiwork');
	$this->load->library('typework_library');
		$this->data['addSuccess']=false;

			$name = $this->input->post('yourName');
			$phone = $this->input->post('phone');
			$email = $this->input->post('yourmail');

			$education = $this->input->post('education');
			$subject = $this->input->post('subject');
			$workexper = $this->input->post('workexper');

			if(!empty($name)&&!empty($phone)){
				$name=trim($name);
		        $name=strip_tags($name);
		        $name=htmlspecialchars($name,ENT_QUOTES,'utf-8');

		        $education=trim($education);
		        $education=strip_tags($education);
		        $education=htmlspecialchars($education,ENT_QUOTES,'utf-8');

		        $subject=trim($subject);
		        $subject=strip_tags($subject);
		        $subject=htmlspecialchars($subject,ENT_QUOTES,'utf-8');

		        $workexper=trim($workexper);
		        $workexper=strip_tags($workexper);
		        $workexper=htmlspecialchars($workexper,ENT_QUOTES,'utf-8');

		        $phone=preg_replace('/[^0-9- ]/', '', $phone);

		        // $this->data['addSuccess']=$this->handiwork->addCooper($name,$phone,$email,$education,$subject,$workexper);
		        $this->data['addSuccess']=true;
				// mail("hello@hellodiplom.ru","Новая заявка на сайте", "Новая заявка на сайте");
				mail("hello@hellodiplom.ru","Заявка на сотрудничество", "Заявка на сотрудничество");
			}

			if($this->data['addSuccess']){
				$this->data['textPage'] = "<img src='/image/inbox.png'/><br/>Спасибо, ваша заявка принята.<br/>В ближайшее время мы свяжемся с вами.";
			}else{
				$this->data['textPage'] = "<img src='/image/sad-face.png'/><br/>При отправке формы произошла ошибка.<br/>Мы уже работаем над ее устранением.";
			}


		$this->dataloc['listType'] = $this->typework_library->listType();
		$this->dataloc['content'] = $this->load->view('form-text',$this->data,true);
		$this->load->view('main',$this->dataloc);

	}

	function sendquestion(){
		if($_GET['name']!=""){
			mail("hello@hellodiplom.ru","Сообщение с Hellodiplom", $_GET['name']." ".$_GET['email']." ".$_GET['question']);
		}else{
			echo "0";
		}

		header("Location: /quesansw");
	}
}