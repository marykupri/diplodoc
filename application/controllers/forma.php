<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Forma extends CI_Controller {

	public function index()
	{
		$this->load->model('typework');
		$this->load->library('typework_library');
		$serv = $this->input->get('serv');
		$this->data['dataType'] = $this->typework->get_data_type($serv);
		$this->data['listType'] = $this->typework_library->listType($serv);
		$this->dataloc['listType'] = $this->typework_library->listType();
		$this->dataloc['content'] = $this->load->view('forma',$this->data, true);
		$this->load->view('main',$this->dataloc);
	}
}