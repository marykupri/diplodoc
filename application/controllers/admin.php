<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct(){
        parent::__construct(); 

        $this->data['id']='';
        $this->data['auth']= '';        
    }

	public function index()
	{	
		$Auth=$this->CheckLog();
	    if($Auth){
			$this->load->model('handiwork');
			$this->load->model('typework');
			$this->load->model('author_model');

			$sort='';
			$sort=$this->input->get('sort');
			$order = $this->input->get('order');

			$srch = $this->input->post('srchBtn');
			if(!empty($srch)){
				$srch = $this->input->post('search');
				$tags = $this->input->post('tags');
				$name = $this->input->post('name');
				$descr = $this->input->post('descr');
				$number = $this->input->post('number');

				if(empty($srch)){
					header("Location: /admin");
					return;
				}

				$quest='';
				$quest='?quest='.$srch;
				if(!empty($tags))
					$quest.="&tags=1";
				if(!empty($name))
					$quest.="&name=1";
				if(!empty($descr))
					$quest.="&descr=1";
				if(!empty($number))
					$quest.="&number=1";

				header("Location: /admin".$quest);
			}

			$quest='';
			$srchQuery=array();
			$quest=$this->input->get('quest');
			if(!empty($quest)){
				$srchQuery['quest']=$quest;
				$namesrch=$this->input->get('name');
				if(!empty($namesrch))
					$srchQuery['namesrch']=$namesrch;
				$tagsrch=$this->input->get('tags');
				if(!empty($tagsrch))
					$srchQuery['tagsrch']=$tagsrch;
				$descrsrch=$this->input->get('descr');
				if(!empty($descrsrch))
						$srchQuery['descrsrch']=$descrsrch;
				$numbersrch=$this->input->get('number');
				if(!empty($numbersrch))
						$srchQuery['numbersrch']=$numbersrch;
			}

			if(!empty($srchQuery))
				$this->data['srchQuery'] = $srchQuery;


			$ord='';
			// if($order=='asc') 
			// 	$ord='order=desc';
			// else
			// 	$ord='order=asc';

			$urlSort=array();
			$urlSort['data']['url']='?sort=data';
			$urlSort['type']['url']='?sort=type';
			$urlSort['name']['url']='?sort=name';
			$urlSort['cena']['url']='?sort=cena';
			$urlSort['razdel']['url']='?sort=razdel';
			$urlSort['author']['url']='?sort=author';
			foreach ($urlSort as $key => $value) {
				if($key==$sort){
					if($order=='asc'){
						$ord='desc';
						$pict='glyphicon-arrow-up';
					}else{
						$ord='asc';
						$pict='glyphicon-arrow-down';
					}						
					$urlSort[$key]['url']='?sort='.$key.'&order='.$ord;
					$urlSort[$key]['pict']=$pict;
				}else{
					$urlSort[$key]['url']='?sort='.$key.'&order=asc';
					$urlSort[$key]['pict']='';
				}
			}
			$this->data['sort'] = $urlSort;


			$txtMsg = '';

			$sbmt=$this->input->post('addWork');
			if(!empty($sbmt)){
				$name = $this->input->post('inputName');
				$descr = $this->input->post('inputDescr');
				$type = $this->input->post('inputType');
				$razdel = $this->input->post('inputRazdel');
				$author = $this->input->post('inputAuthor');
				$tags = $this->input->post('inputPass');
				$cena = $this->input->post('inputCena');

			$uploadfile = '';
	        if(!empty($_FILES['inputFile'])){

	          $FileFoto=trim(basename($_FILES['inputFile']['name']));
	          $FileFoto=mb_strtolower($FileFoto);
	          $fileName=md5($FileFoto.time());

	          $FileSize=basename($_FILES['inputFile']['size']);

	          $uploadfile='';
	          $uploaddir = 'completedWork/';

	        $ext_type = array('pdf','doc');
	        $temp = explode('.', $FileFoto);
	        $ext = $temp[count($temp)-1];
	        $ext=mb_strtolower($ext);

            if($FileSize>0){
                $uploadfile = $uploaddir.$fileName.'.'.$ext;
                if(!empty($uploadfile)){
                    if(file_exists($uploadfile)){
                        unlink($uploadfile);
                    }
                }

                if (!move_uploaded_file($_FILES['inputFile']['tmp_name'], $uploadfile)){
                     $FileFoto='';
                     $uploadfile='';
                }
            }
           else $FileFoto=''; 
	      }else $txtMsg="Не выбран документ для загрузки";
				if(!empty($name)&&!empty($uploadfile)&&empty($txtMsg)){
					$name=trim($name);
			        $name=strip_tags($name);
			        $name=htmlspecialchars($name,ENT_QUOTES,'utf-8');

			        $descr=trim($descr);
			        $descr=strip_tags($descr);
			        $descr=htmlspecialchars($descr,ENT_QUOTES,'utf-8');

			        $tags=trim($tags);
			        $tags=strip_tags($tags);
			        $tags=htmlspecialchars($tags,ENT_QUOTES,'utf-8');

			        $razdel=preg_replace('/[^0-9]/', '', $razdel);
			        $cena=preg_replace('/[^0-9]/', '', $cena);

			        $this->handiwork->addWork($name,$descr,$cena,$tags,$type,$author,$razdel,$uploadfile);
				}
			}

			$this->data['txtMsg'] = $txtMsg;
			// $this->data['sort'] = $sort;
			$listWork = $this->handiwork->getListWorks($id='',$sort,$order,$srchQuery);
			$this->data['listWork'] = $listWork;

			$listSections = $this->handiwork->getListSectn();
			if(!empty($listSections)){
				$optnListSctn='';
				foreach ($listSections as $key => $value) {
					$optnListSctn.= "<option value='$value->id'>$value->nazv</option>";
				}
				$this->data['optnListSctn']=$optnListSctn;
			}

			$listType = $this->typework->getListTypeWork();
			if(!empty($listType)){
				$optnListType='';
				foreach ($listType as $key => $value) {
					$optnListType.= "<option value='$value->id'>$value->nazv</option>";
				}
				$this->data['optnListType']=$optnListType;
			}

			$listAuthor = $this->author_model->get_listAuthor();
			if(!empty($listAuthor)){
				$optnListAuthor='';
				foreach ($listAuthor as $key => $value) {
					$optnListAuthor.= "<option value='".$value['id']."'>".$value['name']."</option>";
				}
				$this->data['optnListAuthor']=$optnListAuthor;				
			}
			
			$this->dataloc['content'] = $this->load->view('/admin/works', $this->data,true);
			$this->load->view('maina', $this->dataloc);
		}else header('location: /admin/login');
	}
	function deltWork($id){

		$this->load->model('handiwork');

		if(!empty($id))
			$this->handiwork->deleteWork($id);

		header('Location: /admin');

	}
	function editWork($id){
		$this->load->model('handiwork');
		$this->load->model('typework');
		$this->load->model('author_model');
		$this->load->library('uploadfile_library');
		$sbmt=$this->input->post('sbmUpd');

		if(!empty($sbmt)){
			$name = $this->input->post('inputName');
			$descr = $this->input->post('inputDescr');
			$razdel = $this->input->post('inputRazdel');
			$type = $this->input->post('inputType');
			$author = $this->input->post('inputAuthor');
			$tags = $this->input->post('inputTags');
			$cena = $this->input->post('inputCena');
			$inShop = $this->input->post('inputShop');

			$fileDel = $this->input->post('inputFileDel');
			$suppDel = $this->input->post('inputSuppFileDel');

			if(!empty($fileDel)){
				$this->handiwork->delFile($id);
			}

			if(!empty($suppDel)){
				$this->handiwork->delSuppFile($id,$suppDel);
			}

			$uploadfile = '';
			$txtMsg = '';

			if(!empty($_FILES['inputFile'])){
	        	$uploaddir = 'completedWork/';
	        	$uplfile = $this->uploadfile_library->uploadFile($_FILES['inputFile'],$uploaddir);
	        }

			//1 - сопроводительные документы
			//2 - внутренние материалы

	        if(!empty($_FILES['suppFile'])){
	        	$uploaddir = 'completedWork/suppDoc/';
	        	$uplfileSupp = $this->uploadfile_library->uploadFile($_FILES['suppFile'],$uploaddir);
	        }

	        if(!empty($_FILES['inputAddit'])){
	        	$uploaddir = 'completedWork/additDoc/';
	        	$uplfileAddit = $this->uploadfile_library->uploadMulti($_FILES['inputAddit'],$uploaddir);
	        }

			if(!empty($name)&&empty($txtMsg)){
				$name=trim($name);
		        $name=strip_tags($name);
		        $name=htmlspecialchars($name,ENT_QUOTES,'utf-8');

		        $descr=trim($descr);
		        $descr=strip_tags($descr);
		        $descr=htmlspecialchars($descr,ENT_QUOTES,'utf-8');

		        $tags=trim($tags);
		        $tags=strip_tags($tags);
		        $tags=htmlspecialchars($tags,ENT_QUOTES,'utf-8');

		        $razdel=preg_replace('/[^0-9]/', '', $razdel);
		        $type=preg_replace('/[^0-9]/', '', $type);
		        $author=preg_replace('/[^0-9]/', '', $author);
		        $cena=preg_replace('/[^0-9]/', '', $cena);

		        $this->handiwork->updWork($id,$name,$descr,$type,$author,$tags,$razdel,$cena,$inShop);
		        if(!empty($uplfile)){
		        	$this->handiwork->addFile($id,$uplfile);
		        }
		        if(!empty($uplfileSupp)){
		        	$this->handiwork->addDoc($id,$uplfileSupp);
		        }
		        if(!empty($uplfileAddit)){
		        	$this->handiwork->addDocMulti($id,$uplfileAddit);
		        }
		        header('Location: /admin');
			}
		}

		$work = $this->handiwork->getListWorks($id);
		$listdoc = $this->handiwork->getListDoc($id);

		if(!empty($listdoc)){
			$docSupp=array();
			$docAddit=array();
			foreach ($listdoc as $key => $value) {
				if($value['typedoc']==1)
					$docSupp[]=$listdoc[$key];
				if($value['typedoc']==2)
					$docAddit[]=$listdoc[$key];
			}

			$this->data['docSupp'] = $docSupp;
	    	$this->data['docAddit'] = $docAddit;
		}

		$listSections = $this->handiwork->getListSectn();
		$optnListSctn='';
	    if(!empty($listSections))
	    foreach ($listSections as $key => $value) {
	      $select = '';
	      if($value->id == $work['razdel']) $select = ' selected';
	      $optnListSctn.= "<option value='$value->id' $select>$value->nazv</option>";
	    }	

		$listType = $this->typework->getListTypeWork();
		$optnListType='';
	    if(!empty($listType))
	    foreach ($listType as $key => $value) {
	      $select = '';
	      if($value->id == $work['type']) $select = ' selected';
	      $optnListType.= "<option value='$value->id' $select>$value->nazv</option>";
	    }		

		$listAuthor = $this->author_model->get_listAuthor();
		$optnListAuthor='';
		    if(!empty($listAuthor))
			    foreach ($listAuthor as $key => $value) {
			      $select = '';
			      if($value['id'] == $work['author']) $select = ' selected';
			      $optnListAuthor.= "<option value='".$value['id']."' $select>".$value['name']."</option>";
			    }

		$this->data['work'] = $work;

    	$this->data['optnListSctn'] = $optnListSctn;
    	$this->data['optnListType'] = $optnListType;
    	$this->data['optnListAuthor'] = $optnListAuthor;

		$this->dataloc['content'] = $this->load->view('/admin/edithwrk', $this->data,true);		
		$this->load->view('maina', $this->dataloc);
	}
	function delFileWork($id,$iddoc){
		$this->load->model('handiwork');
		$this->handiwork->delSuppFile($id,$iddoc);
		header('Location: /admin/editWork/'.$id);
	}
	function seeWork($id){
		$this->load->model('handiwork');
		$this->load->model('typework');
		$sbmt=$this->input->post('sbmUpd');

		$this->data['listWork'] = $this->handiwork->getListWorks($id);
		$this->data['listSections'] = $this->handiwork->getListSectn();
		$this->data['listType'] = $this->typework->getListTypeWork();
		$this->data['listAuthor'] = $this->handiwork->getListAuthor();
		$this->dataloc['content'] = $this->load->view('seewrk', $this->data,true);		
		$this->load->view('maina', $this->dataloc);
	}
	function applicat($id=''){
		$Auth=$this->CheckLog();
	    if($Auth){
			$this->load->model('handiwork');
			$this->load->model('typework');
			$this->load->model('manager_model');
			$this->load->model('author_model');
			$this->load->model('applicat_model');
			$this->load->library('uploadfile_library');

			$srch = $this->input->get('search');
			$sort = $this->input->get('sort');
			$order = $this->input->get('order');

			if(!empty($srch))
				$this->data['search'] = $srch;

			$srchSbm=$this->input->post('srchBtn');
			if(!empty($srchSbm)){
				$srch1 = $this->input->post('search');
				header('Location: /admin/applicat?search='.$srch1);
			}

			$sbmtUpd=$this->input->post('sbmUpd');
			if(!empty($sbmtUpd)){
				$name = $this->input->post('inputName');
				$descr = $this->input->post('inputComm');
				$status = $this->input->post('inputStatus');
				$type = $this->input->post('inputType');
				$author = $this->input->post('inputAuthor');
				$manager = $this->input->post('inputManager');
				$idwork = $this->input->post('inputId');
				$namekl = $this->input->post('inputNamekl');
				$phonekl = $this->input->post('inputPhone');
				$emailkl = $this->input->post('inputEmail');
				$camekl = $this->input->post('inputCameFrom');

				$price = $this->input->post('inputPrice');
				$prepay = $this->input->post('inputPrepay');
				$rest = $this->input->post('inputRest');

				$iddlt = $this->input->post('inputDlt');

				$txtMsg = '';

				if(!empty($iddlt))
					$this->applicat_model->dltFeedBck($idwork);

				if(!empty($name)&&empty($txtMsg)){
					$name=trim($name);
			        $name=strip_tags($name);
			        $name=htmlspecialchars($name,ENT_QUOTES,'utf-8');

			        $descr=trim($descr);
			        $descr=strip_tags($descr);
			        $descr=htmlspecialchars($descr,ENT_QUOTES,'utf-8');

			        $status=preg_replace('/[^0-9]/', '', $status);
			        $type=preg_replace('/[^0-9]/', '', $type);
			        $idwork=preg_replace('/[^0-9]/', '', $idwork);

			        $this->applicat_model->updFeedBck($idwork,$name,$descr,$type,$status,$author,$manager,$namekl,$phonekl,$emailkl,$camekl,$price);

			        if(!empty($price)||!empty($prepay)||!empty($rest)){
			        	$this->applicat_model->priceFeedBck($idwork,$prepay,$rest);
			        }
				}
			}

			$sbmtAdd=$this->input->post('sbmAdd');
			if(!empty($sbmtAdd)){
				$type = $this->input->post('inputType');
				$subject = $this->input->post('inputSubject');
				$theme = $this->input->post('inputTheme');
				$ddline = $this->input->post('inputDdline');
				$str = $this->input->post('inputStr');
				$name = $this->input->post('inputName');
				$phone = $this->input->post('inputPhone');
				$email = $this->input->post('inputEmail');
				$descr = $this->input->post('inputComm');				
				$manager = $this->input->post('inputManager');

				$uplfileOrd=array();
		        if(!empty($_FILES['inputFile'])){
		        	$uploaddir = 'completedWork/orderDoc/';
		        	$uplfileOrd = $this->uploadfile_library->uploadMulti($_FILES['inputFile'],$uploaddir);
		        }

				if(!empty($name)&&!empty($phone)){
					$name=trim($name);
			        $name=strip_tags($name);
			        $name=htmlspecialchars($name,ENT_QUOTES,'utf-8');

			        $descr=trim($descr);
			        $descr=strip_tags($descr);
			        $descr=htmlspecialchars($descr,ENT_QUOTES,'utf-8');

			        // $razdel=preg_replace('/[^0-9]/', '', $razdel);
			        // $type=preg_replace('/[^0-9]/', '', $type);
			        // $idwork=preg_replace('/[^0-9]/', '', $idwork);

			        $idorder = $this->applicat_model->addFeedBck($name,$descr,$type,$manager,$phone,$email,$subject,$theme,$ddline,$str);

			        if(!empty($uplfileOrd)){
			        	if($idorder)
			        		$this->handiwork->addOrderDoc($idorder,$uplfileOrd);
			        }
				}
			}

			$listType = $this->typework->getListTypeWork();
			$listStatus = $this->handiwork->getListStatus();
			$listAuthor = $this->author_model->get_listAuthor();
			$listManager = $this->manager_model->get_listManager();
			$listCameFrom = $this->applicat_model->get_listCameFrom();

			$ord='';
			if($order=='asc') 
				$ord='order=desc';
			else
				$ord='order=asc';

			$urlSort=array();
			$urlSort['id']['url']='?sort=id';
			$urlSort['data']['url']='?sort=data';
			$urlSort['type']['url']='?sort=type';
			$urlSort['email']['url']='?sort=email';
			$urlSort['name']['url']='?sort=name';
			$urlSort['phone']['url']='?sort=phone';
			$urlSort['author']['url']='?sort=author';
			$urlSort['manager']['url']='?sort=manager';
			$urlSort['status']['url']='?sort=status';
			foreach ($urlSort as $key => $value) {
				if($key==$sort){
					if($order=='asc'){
						$ord='desc';
						$pict='glyphicon-arrow-up';
					}else{
						$ord='asc';
						$pict='glyphicon-arrow-down';
					}						
					$urlSort[$key]['url']='?sort='.$key.'&order='.$ord;
					$urlSort[$key]['pict']=$pict;
				}else{
					$urlSort[$key]['url']='?sort='.$key.'&order=asc';
					$urlSort[$key]['pict']='';
				}
			}
			$this->data['sort'] = $urlSort;

			if(!empty($id)){
				$this->data['listFeed'] = $this->applicat_model->getListFeed($id);
				$this->data['priceFeed'] = $this->applicat_model->getPriceFeed($id);
				$orderdoc = $this->handiwork->getListDocOrd($id);
				$this->data['orderdoc'] = $orderdoc;

				$status=$this->data['listFeed']['status'];
				$type=$this->data['listFeed']['typework'];
				$author=$this->data['listFeed']['author'];
				$manager=$this->data['listFeed']['manager'];
				$cameFrom=$this->data['listFeed']['cameFrom'];

				  if(!empty($listStatus)){
				  	$optnListStat='';
				    foreach ($listStatus as $key => $value) {
				      $select = '';
				      if($value->id == $status) $select = ' selected';
				      $optnListStat.= "<option value='$value->id' $select>$value->status</option>";
				    }
				    $this->data['optnListStat']=$optnListStat;
				  }

				  if(!empty($listType)){
				  	$optnListType='';
				    foreach ($listType as $key => $value) {
				      $select = '';
				      if($value->id == $type) $select = ' selected';
				      $optnListType.= "<option value='$value->id' $select>$value->nazv</option>";
				    }
				    $this->data['optnListType']=$optnListType;
				  }

				  if(!empty($listAuthor)){
				  	$optnListAuthor='';
				    foreach ($listAuthor as $key => $value) {
				      $select = '';
				      if($value['id'] == $author) $select = ' selected';
				      $optnListAuthor.= '<option value="'.$value['id'].'"'.$select.'>'.$value['fam'].' '.$value['name'].'</option>';
				    }
				    $this->data['optnListAuthor']=$optnListAuthor;
				  }

				  if(!empty($listManager)){
				  	$optnListManager='';
				    foreach ($listManager as $key => $value) {
				      $select = '';
				      if($value['id'] == $manager) $select = ' selected';
				      $optnListManager.= '<option value="'.$value['id'].'"'.$select.'>'.$value['surname'].' '.$value['name'].'</option>';
				    }
				    $this->data['optnListManager']=$optnListManager;
				  }

				  if(!empty($listCameFrom)){
				  	$optnListCameFrom='';
				    foreach ($listCameFrom as $key => $value) {
				      $select = '';
				      if($value['id'] == $cameFrom) $select = ' selected';
				      $optnListCameFrom.= '<option value="'.$value['id'].'"'.$select.'>'.$value['nazv'].'</option>';
				    }
				    $this->data['optnListCameFrom']=$optnListCameFrom;
				  }


				$this->dataloc['content']=$this->load->view('/admin/editfeedb', $this->data,true);
			}else{

				 if(!empty($listManager)){
				  	$optnListManager='';
				    foreach ($listManager as $key => $value) {
				      $optnListManager.= '<option value="'.$value['id'].'">'.$value['surname'].' '.$value['name'].'</option>';
				    }
				    $this->data['optnListManager']=$optnListManager;
				  }

				  if(!empty($listType)){
				  	$optnListType='';
				    foreach ($listType as $key => $value) {
				      $optnListType.= "<option value='$value->id'>$value->nazv</option>";
				    }
				    $this->data['optnListType']=$optnListType;
				  }

				$this->data['listFeed'] = $this->applicat_model->getListFeed($id='',$sort,$order,$srch);
				$this->dataloc['content']=$this->load->view('/admin/listFeedBck', $this->data,true);
			}

			$this->load->view('maina', $this->dataloc);
		}else header('location: /admin/login');
	}
	//авторизация

	    function checkLog(){
        $id=$this->session->userdata('id');
        $hash=$this->session->userdata('hash');

        if(!empty($hash)&&!empty($id)){
            $str='select id,hash from users where id='.$id;
            $query = mysql_query($str);
            $data = mysql_fetch_assoc($query);

            $this->data['id']=$id;

            if(($data['hash'] != $hash) or ($data['id'] != $id))
                return false;
            else return true;
        }else return false;
    }
    function login(){
        $this->load->model('login');
        if(isset($_POST['submit'])){
          $data = $this->login->authorize($_POST['login']);
            if($data['password'] == $_POST['password']){
                $hash = $this->generateCode(10);
                $hash = md5($hash);

                mysql_query("UPDATE users SET hash='".$hash."' WHERE id='".$data['id']."'");

                $session_data['id']=$data['id'];
                $session_data['hash']=$hash;

                $this->session->set_userdata($session_data);

                header("location: /admin");
            }else 
                print "Вы ввели неправильный логин/пароль";
        }
        
        $this->load->view('login');
    }
     function generateCode($length) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789";
        $code = "";
        $clen = strlen($chars) - 1;  
        while (strlen($code) < $length) {
                $code .= $chars[mt_rand(0,$clen)];  
        }
        return $code;
    }
    function logOut()
    {
      $this->session->set_userdata('id', '0');
      $this->session->set_userdata('hash', '');
      $Auth= false;
      $this->session->sess_destroy();

     header("Location: /admin");
    }
    function textpage($id=''){
		$Auth=$this->CheckLog();
	    if($Auth){
			$this->load->model('handiwork');
			$this->load->model('textpage_model');

			$sort='';
			$sort=$this->input->get('sort');

			$sbmt=$this->input->post('sbmUpd');

		if(!empty($sbmt)){
			// $name = $this->input->post('inputName');
			$descr = $this->input->post('inputComm');
			$id = $this->input->post('inputId');

		$txtMsg = '';

			if(empty($txtMsg)){
		        $descr=trim($descr);
		        $this->textpage_model->updTextPage($id,$descr);
		        header('location: /admin/textpage');
			}
		}
			
			if(!empty($id)){
				$this->data['get_textPage'] = $this->textpage_model->get_textPage($id);			
				$this->dataloc['content']=$this->load->view('/admin/editTextPg', $this->data,true);
			}else{
				$this->data['listFeed'] = '';
				$this->dataloc['content']=$this->load->view('/admin/listTextPage', $this->data,true);
			}
			$this->load->view('maina', $this->dataloc);
		}else header('location: /admin/login');
	}
	function typework($id=''){
		$Auth=$this->CheckLog();
	    if($Auth){
			$this->load->model('handiwork');
			$this->load->model('typework');

			$sbmt=$this->input->post('sbmUpd');
			if(!empty($sbmt)){
				$name = $this->input->post('inputName');
				$group = $this->input->post('inputParent');
				$cena = $this->input->post('inputCena');
				$origin = $this->input->post('inputOriginal');
				$rang = $this->input->post('inputRang');
				$id = $this->input->post('inputId');

		        $this->typework->editListTypeWork($id,$name,$group,$cena,$origin,$rang);
			        header('location: /admin/typework');
			}
			$listGroup = $this->typework->getGroupTypeWork();
			$tempArr=array();
			foreach ($listGroup as $key => $value) {
				$tempArr[$key]['id']=$value->id;
				$tempArr[$key]['name']=$value->name_group;
			}
			$this->data['listGroup']=$tempArr;	

			if(!empty($id)){
				$editWork=array();
				$editListWork = $this->typework->getEditTypeWork($id);
				  if(!empty($editListWork))
				    foreach ($editListWork as $key => $value) {
				      $editWork['nazv']=$value->nazv;
				      $editWork['group']=$value->parent;
				      $editWork['cena']=$value->cena;
				      $editWork['origin']=$value->origin;
				      $editWork['rang']=$value->rang;
				      $editWork['idstr']=$value->id;
				    }
				$this->data['editWork'] = $editWork;
				$this->data['listWork'] = $this->typework->getListTypeWork();			
				$this->dataloc['content']=$this->load->view('editTypeWork', $this->data,true);
			}else{
				$this->data['listWork'] = $this->typework->getListTypeWork($id='');
				$this->dataloc['content']=$this->load->view('listTypeWork', $this->data,true);
			}

			$this->load->view('maina', $this->dataloc);
		}else header('location: /admin/login');
	}
	function addtypework(){
		$Auth=$this->CheckLog();
	    if($Auth){
			$this->load->model('typework');

			$sbmt=$this->input->post('sbmUpd');

		if(!empty($sbmt)){
	
			$name = $this->input->post('inputName');
			$group = $this->input->post('inputParent');
			$cena = $this->input->post('inputCena');
			$origin = $this->input->post('inputOriginal');

	        $this->typework->addTypeWork($name,$group,$cena,$origin);
		        header('location: /admin/typework');
		}

			$listGroup = $this->typework->getGroupTypeWork();
			$tempArr=array();
			foreach ($listGroup as $key => $value) {
				$tempArr[$key]['id']=$value->id;
				$tempArr[$key]['name']=$value->name_group;
			}
			$this->data['listGroup']=$tempArr;
			
			$this->dataloc['content']=$this->load->view('addTypeWork', $this->data,true);
			$this->load->view('maina', $this->dataloc);
		}else header('location: /admin/login');
	}
	function dlttypework($id){
		$Auth=$this->CheckLog();
	    if($Auth){
			$this->load->model('typework');

			$this->typework->dltTypeWork($id);
			header('location: /admin/typework');
		}else header('location: /admin/login');
	}
	function subject(){
		$Auth=$this->CheckLog();
	    if($Auth){
			$this->load->model('handiwork');

			$sbmt=$this->input->post('sbmUpd');
			$this->dataloc['content']='';
			$this->load->view('maina', $this->dataloc);
		}else header('location: /admin/login');
	}
	function manager($action='', $id=''){
		$Auth=$this->CheckLog();
	    if($Auth){
			$this->load->model('manager_model');			

			$sbmt=$this->input->post('sbmUpd');
			if($sbmt){
				$id = $this->input->post('inputId');
				$famil = $this->input->post('inputFam');
				$name = $this->input->post('inputName');
				$phone = $this->input->post('inputPhone');
				$email = $this->input->post('inputEmail');
				$comment = $this->input->post('inputComm');
				if($sbmt=='sbmUpd'){
					$delet = $this->input->post('inputDelete');
					if(empty($delet))
						$this->manager_model->edit_manager($id,$famil,$name,$phone,$email,$comment);
					else
						$this->manager_model->dlt_manager($id);					
				}else{
					$this->manager_model->add_manager($famil,$name,$phone,$email,$comment);
				}
				
			}
			$this->data['listManager'] = $this->manager_model->get_listManager();
			$this->dataloc['content']=$this->load->view('/admin/listManager', $this->data,true);

			if($action=='edit'&&!empty($id)){
				$this->data['dataManager'] = $this->manager_model->get_listManager($id);
				$this->dataloc['content']=$this->load->view('/admin/listManagerEdit', $this->data,true);
			}

			if($action=='add'){
				$this->data['dataManager'] = '';
				$this->dataloc['content']=$this->load->view('/admin/listManagerAdd', $this->data,true);
			}

			$this->load->view('maina', $this->dataloc);
		}else header('location: /admin/login');
	}
	function author($action='', $id=''){
		$Auth=$this->CheckLog();
	    if($Auth){
			$this->load->model('author_model');			

			$sbmt=$this->input->post('sbmUpd');
			if($sbmt){
				$id = $this->input->post('inputId');
				$famil = $this->input->post('inputFam');
				$name = $this->input->post('inputName');
				$phone = $this->input->post('inputPhone');
				$email = $this->input->post('inputEmail');
				$comment = $this->input->post('inputComm');
				if($sbmt=='sbmUpd'){
					$delet = $this->input->post('inputDelete');
					if(empty($delet))
							$this->author_model->edit_author($id,$famil,$name,$phone,$email,$comment);
					else
						$this->author_model->dlt_author($id);					
				}else{
					$this->author_model->add_author($famil,$name,$phone,$email,$comment);
				}				
			}
			$this->data['listAuthor'] = $this->author_model->get_listAuthor();
			$this->dataloc['content']=$this->load->view('/admin/listAuthor', $this->data,true);

			if($action=='edit'&&!empty($id)){
				$this->data['dataAuthor'] = $this->author_model->get_listAuthor($id);
				$this->dataloc['content']=$this->load->view('/admin/listAuthorEdit', $this->data,true);
			}

			if($action=='add'){
				$this->data['dataAuthor'] = '';
				$this->dataloc['content']=$this->load->view('/admin/listAuthorAdd', $this->data,true);
			}

			$this->load->view('maina', $this->dataloc);
		}else header('location: /admin/login');
	}
	function grouptype(){
		$Auth=$this->CheckLog();
	    if($Auth){
			$this->load->model('handiwork');

			$sbmt=$this->input->post('sbmUpd');

			$this->dataloc['content']='';
			$this->load->view('maina', $this->dataloc);
		}else header('location: /admin/login');
	}


}