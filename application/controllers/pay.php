<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pay extends CI_Controller {

	public function index()
	{
		$this->load->model('applicat_model');
		$id=$this->input->get('id');

		$data = $this->applicat_model->dataPaymentOrder($id);
		$dataOrder=array();

		$dataOrder['price'] = $data['pay'];
		$dataOrder['order'] = $data['id'];
		$dataOrder['klient'] = $data['idOrd'];
		$dataOrder['email'] = $data['emailuser'];
		$dataOrder['name'] = $data['nameuser'];
		$dataOrder['theme'] = $data['themeuser'];

		if(!empty($data['data']))
			$this->data['message'] = 'Данный заказ уже оплачен.';
		
		$this->data['dataOrder'] = $dataOrder;
		$this->dataloc['content'] = $this->load->view('paymentStr',$this->data, true);
		$this->load->view('main',$this->dataloc);
	}
}