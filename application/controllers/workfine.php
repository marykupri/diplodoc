<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Workfine extends CI_Controller {

	public function index()
	{
		$this->load->model('handiwork');
		$this->load->model('typework');
		$this->load->library('typework_library');

		$this->data['listType'] = $this->typework_library->listType();
		$this->dataloc['listType'] = $this->typework_library->listType();


		$sbmName= $this->input->post('srchName');
		if(!empty($sbmName)){
			$inputName= $this->input->post('inputName');
			$inputName=trim($inputName);
			$inputName=strip_tags($inputName);
          	$inputName=htmlspecialchars($inputName,ENT_QUOTES,'utf-8');
				$this->data['findDoc'] = $this->handiwork->getFindWork($inputName,'','');
				$this->data['findName'] = $inputName;
		}

		$inputSpec='';
		$sbmType= $this->input->post('srchSbmt');
		if(!empty($sbmType)){
			$inputType= $this->input->post('inputType');
			$inputSpec= $this->input->post('inputSpec');
				$this->data['findDoc'] = $this->handiwork->getFindWork('',$inputType,$inputSpec);
				$this->data['findType'] = $inputType;
				// $this->data['findSpec'] = $inputSpec;
		}

		$noResltSrch='';
		if(isset($this->data['findDoc']))
			if(empty($this->data['findDoc'])){
				$noResltSrch='<div class="no-results">Нет результатов поиска</div>';
			}else{
				$count = count($this->data['findDoc']);
				if(!empty($count)){
					$noResltSrch='<div class="no-results">Найдено '.$count.' документов</div>';
				}
			}

$this->data['listSpec'] = $this->handiwork->getListSpecWork();
		// $listSpec = $this->handiwork->getListSpecWork();
		// $optnListSpec='';
		// if(!empty($listSpec))
		// 	foreach ($listSpec as $key => $value) {
		//     	$class='';
		//     	// if(!empty($inputSpec))&&$value->id==$inputSpec) $class="selected";
		//       // $optnListSpec.= "<option value='".$value['id']."' $class>".$value['nazv']."</option>";
		//     }
		// $this->data['optnListSpec'] = $optnListSpec;

		$this->data['noResltSrch'] = $noResltSrch;
		$this->dataloc['content'] = $this->load->view('workfind',$this->data, true);
		$this->load->view('main',$this->dataloc);
	}
}