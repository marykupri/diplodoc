<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajx extends CI_Controller {

	public function index()
	{
		$this->load->view('welcome_message');
	}


	function get_dataTypeWork(){
		$id = $this->input->get('id');
		$this->load->model('typework');

		$dataType = $this->typework->get_data_type($id);
		
		$data=json_encode($dataType);
        print $data;

		// return $dataType;
	}

	function sendPay(){
		$id=$this->input->post('id');
		$price=$this->input->post('price');
		$email=$this->input->post('email');

		$event = mail($email, 'Сообщение с сайта Hellodiplom', 
			'Для продолжения работы необходимо оплатить '.$price.' рублей. Для оплаты перейдите по ссылке https://hellodiplom.ru/pay?id='.$id);

		if($event)
			print 'Письмо отправлено';
	}
}