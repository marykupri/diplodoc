<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cooperation extends CI_Controller {

	public function index()
	{
		$this->load->model('typework');
		$this->load->library('typework_library');
		$this->load->model('textpage_model');

		$textPage=$this->textpage_model->get_textPage(9);
		foreach ($textPage as $key => $value)
			$this->data['textPage'] = $value->desc;

		// $this->data['dataType'] = $this->typework->get_data_type();
		$this->data['listType'] = $this->typework_library->listType();
		$this->dataloc['listType'] = $this->typework_library->listType();
		$this->dataloc['content'] = $this->load->view('cooperation_views',$this->data, true);
		$this->load->view('main',$this->dataloc);
	}
}