<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class FailUrl extends CI_Controller {

	public function index()
	{

		$this->data['textPage'] = "<img src='/image/sad-face.png'/><br/>Произошла ошибка, попробуйте повторить попытку позже.";

		$this->dataloc['content'] = $this->load->view('form-text',$this->data,true);
		$this->load->view('main',$this->dataloc);
	}
}
?>