<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Guarantee extends CI_Controller {

	public function index()
	{
		$this->load->model('textpage_model');
		$this->load->library('typework_library');
		
		$textPage=$this->textpage_model->get_textPage(6);
		foreach ($textPage as $key => $value) {
			$this->data['textPage'] = $value->desc;
			$this->data['titlePage']= $value->name;
		}
		$this->dataloc['listType'] = $this->typework_library->listType();
		$this->dataloc['content'] = $this->load->view('textpage',$this->data, true);
		$this->load->view('main',$this->dataloc);
	}
}