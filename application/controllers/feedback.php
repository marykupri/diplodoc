<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Feedback extends CI_Controller {
	public function index()
	{	
		$this->load->database();
		$this->load->model('handiwork');

		$txtMsg = '';		

		$sbmt=$this->input->post('insFeed');
		if(!empty($sbmt)){
			$name = $this->input->post('inputName');
			$phone = $this->input->post('inputPhone');
			$email = $this->input->post('inputEmail');
			$typeWrk = $this->input->post('inputType');
			$theme = $this->input->post('inputTheme');

			$uploadfile = '';
	        if(!empty($_FILES['inputFile'])){

	          $FileFoto=trim(basename($_FILES['inputFile']['name']));
	          $FileFoto=mb_strtolower($FileFoto);
	          $fileName=md5($FileFoto.time());

	          $FileSize=basename($_FILES['inputFile']['size']);

	          $uploadfile='';
	          $uploaddir = 'usersDoc/';

	        $ext_type = array('pdf','doc','txt');
	        $temp = explode('.', $FileFoto);
	        $ext = $temp[count($temp)-1];
	        $ext=mb_strtolower($ext);

	        if(in_array($ext, $ext_type)){
	           if($FileSize>0)
	           {
	                $uploadfile = $uploaddir.$fileName.'.'.$ext;
	                if(!empty($uploadfile)){
	                        if(file_exists($uploadfile)){
	                            unlink($uploadfile);
	                        }
	                      }

	                if (!move_uploaded_file($_FILES['inputFile']['tmp_name'], $uploadfile)){
	                     $FileFoto='';
	                     $uploadfile='';
	                  }
	           }
	           else $FileFoto=''; 
	        }
	        else{
	          $txtMsg="Документ не правильного фората";
	        }
	      }else $txtMsg="Не выбран документ для загрузки";

			if(!empty($name)&&!empty($phone)){
				$name=trim($name);
		        $name=strip_tags($name);
		        $name=htmlspecialchars($name,ENT_QUOTES,'utf-8');

		        $theme=trim($theme);
		        $theme=strip_tags($theme);
		        $theme=htmlspecialchars($theme,ENT_QUOTES,'utf-8');

		        $phone=preg_replace('/[^0-9- ]/', '', $phone);

		        $this->handiwork->addFeed($name,$phone,$email,$typeWrk,$theme,$uploadfile);
			}
		}
		
		$this->data['txtMsg'] = $txtMsg;
		$this->data['listType'] = $this->handiwork->getListTypeWork();
		$this->dataloc['content'] = $this->load->view('feedback', $this->data,true);
		$this->load->view('maina', $this->dataloc);
	}
}