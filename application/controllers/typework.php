<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Typework extends CI_Controller {

	public function index()
	{
		$this->load->model('textpage_model');
		$textPage=$this->textpage_model->get_textPage(2);
		foreach ($textPage as $key => $value) {
			$this->data['textPage'] = $value->desc;
			$this->data['titlePage'] = $value->name;
		}
		$this->dataloc['content'] = $this->load->view('textpage',$this->data, true);
		$this->load->view('main',$this->dataloc);
	}
}