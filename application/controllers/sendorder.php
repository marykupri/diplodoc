<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sendorder extends CI_Controller {

	public function index()
	{
		$this->load->model('handiwork');
		$this->load->library('typework_library');
		$this->load->library('uploadfile_library');
		$this->data['addSuccess']=false;

			$name = $this->input->post('yourName');
			$phone = $this->input->post('phone');
			$email = $this->input->post('yourmail');
			$typeWrk = $this->input->post('BusinessDistrictId');
			$specWrk = $this->input->post('nameSpecialty');
			$inputMsg = $this->input->post('msg');
			$theme = $this->input->post('name-theme');
			$data = $this->input->post('yourname');

			$uplfileOrd=array();
	        if(!empty($_FILES['inputFile'])){
	        	$uploaddir = 'completedWork/orderDoc/';
	        	$uplfileOrd = $this->uploadfile_library->uploadMulti($_FILES['inputFile'],$uploaddir);
	        }

			if(!empty($name)&&!empty($phone)){
				$name=trim($name);
		        $name=strip_tags($name);
		        $name=htmlspecialchars($name,ENT_QUOTES,'utf-8');

		        $theme=trim($theme);
		        $theme=strip_tags($theme);
		        $theme=htmlspecialchars($theme,ENT_QUOTES,'utf-8');

		        $phone=preg_replace('/[^0-9- ]/', '', $phone);

		        $this->data['addSuccess']=$this->handiwork->addFeed($name,$phone,$email,$typeWrk,$specWrk,$theme,$inputMsg,$data);

		        if(!empty($uplfileOrd)){
		        	$this->handiwork->addOrderDoc($this->data['addSuccess'],$uplfileOrd);
		        }

		        $headers = "From:hello@hellodiplom.ru";

				mail("hello@hellodiplom.ru","Новая заявка на сайте", "Новая заявка на сайте");
				mail($email,"Оформление заявки с сайта Hellodiplom", "Вы оформили заказ №".$this->data['addSuccess'].". 
																	  В ближайшее время менеджер свяжется с вами",$headers);
			}

			if($this->data['addSuccess']){
				$this->data['textPage'] = "<img src='/image/inbox.png'/><br/>Спасибо, что доверяете нам, ваша заявка принята.<br/>В ближайшее время менеджер свяжется с вами.";
			}else{
				$this->data['textPage'] = "<img src='/image/sad-face.png'/><br/>При отправке формы произошла ошибка.<br/>Мы уже работаем над ее устранением.";
			}


		$this->dataloc['listType'] = $this->typework_library->listType();
		$this->dataloc['content'] = $this->load->view('form-text',$this->data,true);
		$this->load->view('main',$this->dataloc);

	}

	function sendquestion(){
		if($_GET['name']!=""){
			mail("hello@hellodiplom.ru","Сообщение с Hellodiplom", $_GET['name']." ".$_GET['email']." ".$_GET['question']);
		}else{
			echo "0";
		}

		header("Location: /quesansw");
	}
}