<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Quesansw extends CI_Controller {

	public function index()
	{
		$this->load->model('textpage_model');
		$this->load->library('typework_library');

		$textPage=$this->textpage_model->get_textPage(8);
		foreach ($textPage as $key => $value) {
			$this->data['textPage'] = $value->desc;
			$this->data['titlePage']= $value->name;
		}
		$this->dataloc['listType'] = $this->typework_library->listType();
		$this->dataloc['content'] = $this->load->view('questansw',$this->data, true);
		$this->load->view('main',$this->dataloc);
	}
}
?>